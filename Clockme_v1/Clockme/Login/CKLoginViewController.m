//
//  ViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKLoginViewController.h"

@interface CKLoginViewController (){
    CKCoreWebService *CK;
    UIAlertView *success,*altPassword,*resetPassword;
    NSTimer *animation;
    NSUserDefaults *load;
    int count,atempts;
    BOOL limit;
}

@end

@implementation CKLoginViewController

@synthesize txtEmail,txtPassword,lblVersion;
@synthesize imgBg;
@synthesize scrollView;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    
    load = [NSUserDefaults standardUserDefaults];
    atempts = 0;
    //[self InitAnimation];
    
    [txtPassword setDelegate:self];
}

/**
 Load email if user has email on device memory
 
 @param animated default
 */
-(void)viewDidAppear:(BOOL)animated{
    
    //Define the label with the current version and build
    lblVersion.text = [NSString stringWithFormat:@"Version: %@b%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];

    //Try to load the user again
    [[CKPersonModel alloc] initCKPersonFromMemory];
    if (CKPerson.email.length != 0) {
        txtEmail.text = CKPerson.email;
    }else{
        txtEmail.text = [load valueForKey:@"userEmail"];
    }
    
    //Show alert message if user has been logff due a token expired
    if ([load valueForKey:kUserTokenExpired]) {
        [load removeObjectForKey:kUserTokenExpired];
        [[[UIAlertView alloc] initWithTitle:nil message:MSG_LOGOFF_TOKEN_ERROR delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

/**
 Method to start login
 
 @param sender default
 */
-(void)Login:(id)sender{
    [self Background:self];
    
    if (txtEmail.text.length != 0 && txtPassword.text.length != 0) {
        @try {
            
            [[CKViewController sharedInstance] showClockInView:self.view];
            [self performSelectorInBackground:@selector(RequestUserLogin) withObject:nil];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Error: %@",exception);
        }
        
    }else{
        [[[UIAlertView alloc] initWithTitle:@"" message:MSG_REGISTER_ALL_FIELDS delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

/**
 Method to request login
 */
-(void)RequestUserLogin{
    
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@&password=%@",txtEmail.text,txtPassword.text];
    [CK CoreWebService:kRouteUserLogin withRequest:myRequestString];
}

/**
 Method to request a password reset
 */
-(void)RequestForgetPassword{
    
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@",txtEmail.text];
    [CK CoreWebService:kRouteUserForgotPassword withRequest:myRequestString];

}


/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginSuccess) {
            
            //[[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kSBUserSessionActive];
            [[CKLoginModel sharedInstance] LoginSucess:dictionary];
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }else{
                [self presentViewController:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"HomeView"] animated:YES completion:nil];
            }
                        
        }else{
            
            //If forget password
            if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginForgotPassword) {
                [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }else{
                //[[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            
        }
        
    }else{
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginFail) {
            atempts++;
            if (atempts > 2) {
                atempts = 0;
                [self ForgetPassword:self];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                });
            }
        }
        
    }
    
    [[CKViewController sharedInstance] removeClock];
}

/**
 Method to force user to change their password
 */
-(void)ForcePasswordChange{
    
    altPassword = [[UIAlertView alloc] initWithTitle:@"Password Change" message:@"Type new password and confirm" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [altPassword setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    //Custom alert
    [[altPassword textFieldAtIndex:0] setSecureTextEntry:YES];
    [[altPassword textFieldAtIndex:0] setPlaceholder:@"Password"];
    
    [[altPassword textFieldAtIndex:1] setSecureTextEntry:YES];
    [[altPassword textFieldAtIndex:1] setPlaceholder:@"Repeat Password"];
    
    [altPassword show];
    
}

/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{

    [[CKViewController sharedInstance] removeClock];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

/**
 Method to send user forgot password, it ask him to enter the email on the textfield and tap on the button
 
 @param sender default
 */
-(IBAction)ForgetPassword:(id)sender{
    
    resetPassword = [[UIAlertView alloc] initWithTitle:@"" message:MSG_LOGIN_RESET_PASSWORD delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    [resetPassword show];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (success) {
        if (buttonIndex == 0) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
        success = nil;
    }else if (resetPassword){
        
        if (buttonIndex == 1) {
            if (txtEmail.text.length == 0) {
                
                [[[UIAlertView alloc] initWithTitle:@"" message:MSG_LOGIN_PASSWORD_NO_EMAIL delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                
            }else{
                
                [[CKViewController sharedInstance] showClockInView:self.view];
                [self performSelectorInBackground:@selector(RequestForgetPassword) withObject:nil];
            }
        }
        
        resetPassword = nil;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self Login:self];
    [txtPassword resignFirstResponder];
    
    NSLog(@"pressed go button");
    return TRUE;
}

/**
 Method when click background to dissaper with keyboard
 
 @param sender default
 */
-(IBAction)Background:(id)sender{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[CKViewController sharedInstance] removeClock];
}

@end
