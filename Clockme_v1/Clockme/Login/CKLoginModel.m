//
//  LoginModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 14/01/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKLoginModel.h"

@implementation CKLoginModel{
    CKCoreWebService *CK;
}

static CKLoginModel *sharedInstance = nil;

+(CKLoginModel *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

/**
 Method to load all data from device memory
 */
-(void)BeginCKSession{
    
    //Check if app change version and update the database
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
        if (![self UpdateAppVersion]) {
            [self RestoreSession];
        }
    }
}

/**
 *  Restore from memory from inside the class
 */
-(void)RestoreSession{
        
    //Load info from memory
    [[CKPersonModel alloc] initCKPersonFromMemory];
    [[CKPlaceModel alloc] initSBSiteFromMemory];
    [[CKCompanyModel alloc] initSBEntityFromMemory];
}

/**
 Method to save all user data when login is sucessful
 
 @param dictionary user information
 */
-(void)LoginSucess:(NSDictionary *)dictionary{
        
    @try {
        
        CKPerson = [[CKPersonModel alloc] initObjectWithDictionary:[[NSArray arrayWithObjects:dictionary,nil] objectAtIndex:0]];
        [CKPerson initSBSession];
        [CKPersonModel UpdateUserInfo];
        
        CKCompany = [[CKCompanyModel alloc] initObjectWithDictionary:[[NSArray arrayWithObjects:dictionary,nil] objectAtIndex:0]];
        [CKCompanyModel UpdateCompanyInfo];
        
        CKPlace = [[CKPlaceModel alloc] init];
        
        //Update user device token
        [self RegisterDeviceToken:[[NSUserDefaults standardUserDefaults] valueForKey:kUserDeviceToken]];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)LogOffUserWithAccessDenied:(UIViewController *)view{
    
    //Destroy session
    CKPerson.session = FALSE;
    CKPerson.token = nil;
    [CKPersonModel UpdateUserInfo];
    
    @try {
        [view presentViewController:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"loginView"] animated:YES completion:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

/**
 Methdo to update user device token
 
 @return default
 */

-(void)RegisterDeviceToken:(NSData *)deviceToken{
    
    CKPerson.deviceToken = [[[[deviceToken description]
                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSString *type =  [[UIDevice currentDevice] systemName];
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    
    if (CKPerson.deviceToken == NULL) {
        CKPerson.deviceToken = @"";
    }
    
    NSLog(@"Update Device token: %@",CKPerson.deviceToken);
    NSLog(@"Update type: %@",type);
    NSLog(@"Update Timezone: %@",tz.name);
    
    NSString *version = [NSString stringWithFormat:@"%@b%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    double currTime = [[NSDate date] timeIntervalSince1970];
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&deviceId=%@&deviceType=%@&dateModified=%f&timezone=%@&version=%@",CKPerson.personId,CKPerson.token,CKPerson.deviceToken,type,currTime,tz.name,version];
    [[CKCoreWebService sharedInstance] CoreWebService:kRouteUserUpdateDevice withRequest:myRequestString];
    
}

/**
 Method to update app version
 */
-(BOOL)UpdateAppVersion{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *currentVersion = [userDefault valueForKey:kAppVersion];
    NSString *version = [NSString stringWithFormat:@"%@b%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    if (![currentVersion isEqualToString:version]) {
        
        //Init the core variable
        CK = [[CKCoreWebService alloc] init];
        [CK setDelegate:self];
        
        //Backup app data
        NSMutableDictionary *arrayCKPerson = [userDefault objectForKey:kCKPersonModel];
        NSMutableDictionary *arraySBSite = [userDefault objectForKey:kCKPlaceModel];
        
        //Remove all saved keys to start fresh on the new version
        [userDefault setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
        
        //Restore app data
        [userDefault setObject:arrayCKPerson forKey:kCKPersonModel];
        [userDefault setObject:arraySBSite forKey:kCKPlaceModel];
        
        [userDefault setValue:version forKey:kAppVersion];
        [userDefault setValue:@"1" forKey:kFirstRun];
        
        //Restore all the information before proceeding
        [self RestoreSession];

        if (CKPerson.session && CKPerson.personId != 0) {
            
            //Load device location status
            CLAuthorizationStatus locationStatus = [CLLocationManager authorizationStatus];
            int pushStatus = 0;
            
            if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8) {
                pushStatus = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
            }else{
                UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
                if (types == UIRemoteNotificationTypeAlert){
                    pushStatus = 1;
                }
            }

            NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&version=%@&location=%i&push=%i",CKPerson.personId,CKPerson.token,version,locationStatus,pushStatus];
            [CK CoreWebService:kRouteUserUpdateVersion withRequest:myRequestString];
        }
        
        return TRUE;
    }
    
    return FALSE;
}

-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginSuccess) {

            NSLog(@" :::::::: Updated version on cloud :::::::::");
            [self LoginSucess:dictionary];
            
        }
    }
}

-(void)DidFinishDownloadingWithError:(NSString *)error{
    NSLog(@"Error: %@",error);
}

@end
