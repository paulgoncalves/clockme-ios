//
//  SBWebService.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 16/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#ifndef SafetyBeat_SBWebService_h
#define SafetyBeat_SBWebService_h

////////////////////////////////////
/////////  WS ROUTE  ///////////////
////////////////////////////////////

//////////// PERSON ////////////
//user login
#define kRouteUserLogin @"login"
//user forgot password
#define kRouteUserForgotPassword @"forgotPassword"
//user registration
#define kRouteUserRegister @"registerUser"
//user update info
#define kRouteUserUpdate @"updateUser"
//user update password
#define kRouteUserUpdatePassword @"updateUserPassword"
//update device
#define kRouteUserUpdateDevice @"updateDevice"
//update version
#define kRouteUserUpdateVersion @"updateAppVersion"
//update timezone
#define kRouteUserUpdateTimezone @"updateTimezone"


//////////// PLACE ////////////
//get all sites available for user for entity
#define kRoutePlaceCompany @"companyPlace"
//get site by id
#define kRouteSite @"site"
//Create site
#define kRouteSiteCreate @"createSite"
//update site
#define kRouteSiteUpdate @"updateSite"
//delte site
#define kRouteSiteDelete @"deleteSite"
//Site Project
#define kRouteSiteLog @"siteLog"

//////////// ACTION ////////////
//checkin
#define kRouteActionClockin @"clockIn"
//checkout
#define kRouteActionClockout @"clockOut"
//Person actions
#define kRoutePersonActions @"personActions"
//Fetch action notes
#define kRouteNote @"siteNote"
//Save action note
#define kRouteActionNote @"saveActionNote"

//////////// MY TEAM ////////////
//Get all team for entity
#define kRouteTeamMyTeam @"myTeam"
//get all team leader by entity
#define kRouteTeamMyLeader @"myTeamLeader"
//Get all user team leaders
#define kRouteTeamAllTeam @"teamLeader"

//////////// LOG ////////////
//Save log to log.txt
#define kRouteLog @"log"
//Save to actionLog table
#define kRouteLogAction @"logAction"
//Send log to support
#define kRouteLogToSupport @"sendSupportLog"

//////////// UPLOAD ////////////
//Save to actionLog table
#define kRouteImgUpload @"imgUpload"

//////////// WEB SERVICE RESPONSE KEYS ////////////
#define WebserviceKeyStatus @"status"
#define WebserviceKeyMsgStatus @"msgStatus"
#define WebserviceKeyMessage @"message"
#define WebserviceKeyCode @"WSResponseCode"

/////////////// SPECIAL ENUMERATION ///////////////
//Webservice response status
typedef enum{
    SBWebServiceResponseSuccess=1,
    SBWebServiceResponseFail,
    SBWebServiceResponseUnknown
}SBWebServiceResponseStatus;

//Webservice response code
typedef enum{
    SBWebServiceResponseCode_LoginSuccess=100,
    SBWebServiceResponseCode_LoginFail=101,
    SBWebServiceResponseCode_LoginNotActive=102,
    SBWebServiceResponseCode_LoginForgotPassword=103,
    SBWebServiceResponseCode_LoginInvalidEmail=104,
    SBWebServiceResponseCode_PersonRegisterSuccess=110,
    SBWebServiceResponseCode_PersonRegisterFail=111,
    SBWebServiceResponseCode_PersonRegisterExistent=112,
    SBWebServiceResponseCode_PersonUpdateSuccess=113,
    SBWebServiceResponseCode_PersonUpdateFail=114,
    SBWebServiceResponseCode_PersonUpdatePasswordSuccess=115,
    SBWebServiceResponseCode_PersonUpdatePasswordFail=116,
    SBWebServiceResponseCode_PersonStatusSuccess=117,
    SBWebServiceResponseCode_PersonStatusFail=118,
    SBWebServiceResponseCode_PlaceFetchSuccess=130,
    SBWebServiceResponseCode_PlaceFetchFail=131,
    SBWebServiceResponseCode_PlaceCreateSuccess=132,
    SBWebServiceResponseCode_PlaceCreateFail=133,
    SBWebServiceResponseCode_PlaceCreateFailUpgrade=134,
    SBWebServiceResponseCode_PlaceUpdateSuccess=135,
    SBWebServiceResponseCode_PlaceUpdateFail=136,
    SBWebServiceResponseCode_PlaceDeleteSuccess=137,
    SBWebServiceResponseCode_PlaceDeleteFail=138,
    SBWebServiceResponseCode_PlaceLogFetchSuccess=150,
    SBWebServiceResponseCode_PlaceLogFetchNoLog=151,
    SBWebServiceResponseCode_PlaceLogFetchFail=152,
    SBWebServiceResponseCode_ActionNoteSaveSuccess=153,
    SBWebServiceResponseCode_ActionNoteSaveFail=154,
    SBWebServiceResponseCode_ActionNoteSuccess=155,
    SBWebServiceResponseCode_ActionNoteFail=156,
    SBWebServiceResponseCode_ActionNoteNoNote=157,
    SBWebServiceResponseCode_ActionClockinSuccess=171,
    SBWebServiceResponseCode_ActionClockinFail=172,
    SBWebServiceResponseCode_ActionClockoutSuccess=173,
    SBWebServiceResponseCode_ActionClockoutFail=174,
    SBWebServiceResponseCode_MyTeamSuccess=200,
    SBWebServiceResponseCode_MyTeamFail=201,
    SBWebServiceResponseCode_MyTeamNoTeam=202,
    SBWebServiceResponseCode_MyTeamNoLeader=204,
    SBWebServiceResponseCode_MyTeamLeaderFetchSuccess=205,
    SBWebServiceResponseCode_MyTeamLeaderNoLeader=206,
    SBWebServiceResponseCode_LogUserSuccess=210,
    SBWebServiceResponseCode_LogUserFail=211,
    SBWebServiceResponseCode_LogNoLog=212,
    SBWebServiceResponseCode_ImageSuccess=250,
    SBWebServiceResponseCode_ImageFail=251,
    SBWebServiceResponseCode_ImageInvalid=252,
    SBWebServiceResponseCode_LogSuccess=260,
    SBWebServiceResponseCode_LogFail=261,
    SBWebServiceResponseCode_LogEntityUpdateByUser=262,
    SBWebServiceResponseCode_LogEntityUpdateBySite=264,
    SBWebServiceResponseCode_UserTokenUpdateSuccess=350,
    SBWebServiceResponseCode_UserTokenUpdateFail=351,
    SBWebServiceResponseCode_UserVersionUpdateSuccess=352,
    SBWebServiceResponseCode_UserVersionUpdateFail=353,
    SBWebServiceResponseCode_LoginAccessDenied=500,
    SBWebServiceResponseCode_PDOError=501
}SBWebServiceResponseCode;

#endif
