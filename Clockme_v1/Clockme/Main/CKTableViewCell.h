//
//  EntityTableViewCell.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIView *viewBody;
@property (nonatomic, strong) IBOutlet UIView *viewHeader;

@property (nonatomic, weak) IBOutlet UIImageView *imgAction;
@property (nonatomic, weak) IBOutlet UILabel *lblHeaderTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblHeaderSubtitle;

@property (nonatomic, weak) IBOutlet UILabel *lblFooterTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblFooterSubtitle;

@property (nonatomic, weak) IBOutlet UITextView *txtActionNote;

@property (nonatomic, weak) IBOutlet UIButton *btnAction;

@end
