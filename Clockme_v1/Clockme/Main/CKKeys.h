//
//  SBKeys.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 16/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

//ASSETS SERVER
#define MAIN_HOST @"http://www.clockme.co"
#define IMG_HOST @"http://clockme.co/assets"

///////////////  VERSION 2.4.1  ///////////////////////
#define HOST @"api.clockme.co/v1"
//#define HOST @"192.168.0.12/clockme/api/v1"

////////////////////////////////////
/////////////// KEYS ///////////////
////////////////////////////////////

//Default constatns
#define IS_IPHONE_BIG ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define BATTERY_LEVEL ([[UIDevice currentDevice] batteryLevel] * 100)
#define KEYBOARD_HEIGHT 210
#define SECONDS_IN_AMINUTE 60
#define SECONDS_IN_AHOUR 3600
#define SECONDS_IN_ADAY 86400
#define MINUTES_IN_AHOUR 60

#ifndef SafetyBeat_SBKeys_h
#define SafetyBeat_SBKeys_h

//LOCATION
#define ALLOWED_RADIUS 250
#define ACCEPTED_RADIUS 800
#define DISTANCE_FROM_ME 800
#define DEFAULT_RADIUS 50

//Simulator coordination for alerton office
#define fakeLat -37.702323
#define fakeLng 145.095682

//Company
#define kCKCompanyModel @"CKCompanyModel"

//User
#define kCKPersonModel @"CKUserModel"
#define kUserDeviceToken @"userDeviceToken"
#define kUserTokenExpired @"userTokenExpired"

//Site
#define kCKPlaceModel @"CKSiteModel"

//General
#define kUsedTime @"usedTime"
#define kAppVersion @"appVersion"
#define kFirstRun @"firstRun"
#define kAppLauchTimeStamp @"lauchTimeStamp"

//SOUND
#define kDefault_sound @"default"

//STORYBOARD
#define kStoryBoardMain @"Main"
#define kStoryBoardSmall @"Main35"

/////////////// SPECIAL ENUMERATION ///////////////
//User access level
typedef enum{
    CKUserAccessLevelManager=1,
    CKUserAccessLevelNormal
}accessLevel;

#endif
