//
//  Constants.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKMain.h"

CKPersonModel *CKPerson;
CKPlaceModel *CKPlace;
CKCompanyModel *CKCompany;

@implementation CKMain

+(UIStoryboard *)getStoryBoard{

    UIStoryboard *st = [UIStoryboard storyboardWithName:kStoryBoardMain bundle:nil];
    return st;
}

@end