//
//  EntityTableViewCell.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKTableViewCell.h"

@implementation CKTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
