//
//  Constants.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 19/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

//Main classes
#import "CKMenuViewController.h"
#import "CKViewController.h"

//Core classes
#import "CKCoreWebService.h"
#import "CKCoreLocation.h"
#import "CKCoreTime.h"
#import "CKColor.h"
#import "CKAnimation.h"
#import "CKKeys.h"
#import "CKWebService.h"
#import "CKLang.h"
#import "CKViewController.h"
#import "CKUtility.h"
#import "CKActionModel.h"

//SB models
#import "CKLoginModel.h"
#import "CKPersonModel.h"
#import "CKPlaceModel.h"
#import "CKCompanyModel.h"

//Global variables
extern CKPersonModel *CKPerson;
extern CKPlaceModel *CKPlace;
extern CKCompanyModel *CKCompany;

@interface CKMain : NSObject

+(UIStoryboard *)getStoryBoard;

@end
