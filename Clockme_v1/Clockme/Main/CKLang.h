//
//  SBMessage.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 16/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#ifndef Clockme_CKMessage_h
#define Clockme_CKMessage_h


////////////////////////////////////
//////////// MESSAGES //////////////
////////////////////////////////////

/////////////// APP EMBEDED WORDS ///////////////



/////////////// GENERAL MESSAGE ///////////////
// Message will be display when user first login to app using the signup assistance
#define MSG_REGISTER_WELCOME @"Welcome to Clockme"
// Popup alert when device don't support email feature
#define MSG_SEND_EMAIL_ERROR @"Device don't support email feature"
// Popu alert when device don't support SMS feature
#define MSG_SMS_NOT_SUPPORTED @"Error occur, device dont support SMS"

/////////////// INTERNET CONNECTION ISSUES ///////////////
//Message displayed when the device dont have any kind of internet available
#define MSG_NO_INTERNET @"No Internet Connection Available"
// Message displayed when not able to access the webservice.
// Normally when there is a programmer code error that need fix
// But also could be a server issue or
// Miss of package delivery from server to device
#define MSG_FETCH_ERROR @"A problem occur fetching data, please try again later"
// Message displayed to user when the checkin time has expired and user has no internet available
#define MSG_ACTION_LOCAL_PUSH @"It seems you don't have internet, please follow the company procedure to report-in."


/////////////// LOCATION ////////////////
// Message will display a popup alert when location is disable on the phone, this message will be persistance, every time the home screen is loaded until user enable it back.
#define MSG_LOCATION_DISABLE @"Please allow location service on:\nSetting > Privacy > Location Services > \n and enable SafetyBeat."
// Message displayed on home view red flashing banner when location is disable on the phone
#define MSG_LOCATION_DISABLE_BANNER @"LOCATION SERVICE DISABLED."
// Message displayed on home view Alerting about the limitation of the app functions
#define MSG_LOCATION_ALWAYS_DISABLE_BANNER @"App with limitations, enable always location."
// Popup alert when user try to login to site when his distance to site is more then site radius + accepted distance
#define MSG_USER_NO_RANGE @"You have to be at least %@ meters from the site to check-in"
// Message displayed in banner when user distance from site is more then site radius + allowed distance
#define MSG_USER_LEFT_SITE_BANNER @"It seems that you left %@."
// Popup alert when user has confirmed he left the site radius + allowed distance
// It requires user to confirm before checkin out user from site
#define MSG_USER_LEFT_SITE_CHECKOUT @"You have been checked out from %@ because you are out of site radius"
// Popup alert when user try to create a site with lat or lng equal to 0
#define MSG_LOCATION_SITE_INVALID @"Site location is invalid, try again"
// Popup alert when user try to send action with invalid coordinate
#define MSG_LOCATION_USER_INVALID @"Your location doesn't seems valid, please try again"


/////////////// VALIDATION /////////////////
// Popup alert when user atempt to send a form with required field left empty
#define MSG_REGISTER_ALL_FIELDS @"All fields must be filled"
// Popup alert when user atempt to register a invalid email format
#define MSG_REGISTER_INVALID_EMAIL @"Invalid email address"
// Popup alert when user input a diferent confirmation password when registering to start using the app
#define MSG_REGISTER_PASSWORD_NO_MATCH @"Passwords don't match"
// Popup alert when user click on join entity without typing the entity code
#define MSG_REGISTER_NO_CODE @"Please enter the entity code"
// Popup alert when user try to login without input the email address
#define MSG_LOGIN_PASSWORD_NO_EMAIL @"Please enter your email above"
// Popup alert when user try to click send answers and still some question withou answer
#define MSG_ANSWER_ALL @"Please answer all the questions before continue"
// Popup alert when user atempt to register without accepting the terms and conditions
#define MSG_REGISTER_ACCEPT_TERMS_CONDITIONS @"Please read and accept the terms and conditions"
// Popup alert to ask user to describe the action their taken when acting for someone in danger
// The action can be: User is safe or Can't help.
#define MSG_PUSH_BRIEF @"Please describe your action"
// Popup alert when user try to create a leave without having leave type
#define MSG_LEAVE_NO_SUPPORTED @"Error occur, the entity doesn't support leave"
// Popup alert when user atempt to clear man down alert
#define MSG_MAN_DOW_DEACTIVE @"Please enter your password to clear the Man Down alarm"


/////////////// CONFIRMATION /////////////////
// Popup alert to confirm if user wants to reset the password that will be sent to their email
#define MSG_LOGIN_RESET_PASSWORD @"Do you want to reset your password?"
// Popup alert to confirm for site deletion
#define MSG_SITE_DEACTIVATE @"Are you sure you want delete this site?"
// Popup alert to confirm if user wants to continue to checkin even when it seems that he is not on site radius + allowed distance
#define MSG_USER_ACCEPTABLE_RANGE @"It appears you're not within %@ radius. Check-in anyway?"
// Popup alert to confirm if user wants to checkout from site when it was identified that he whent out of site radius + accepted distance
#define MSG_USER_LEFT_SITE @"It seems that you left %@, if so, please check-out"
// Popup alert to confirm if user that is taken action for someone in danger is sure about the safety of the user
#define MSG_PUSH_REPORT @"Are you sure the user is safe?"
// Popup alert to confirm if user that is taken action for someone in danger is sure he wants to ignore the alert
#define MSG_PUSH_IGNORE @"Are you sure you want to ignore this emergency request?"
// Popup alert to confirm if user has finish for today when he presses the buttion checkout
#define MSG_CHECKOUT_FINISH @"Are you finished work for today?"
// Popup alert to confirm if user wants to remove the currently connection
#define MSG_CONNECTION_REMOVE @"Are you sure you want to disconnect from %@"
// Popup alert to confirm if user wants to set the other user as team leader
#define MSG_CONNECTION_TEAMLEADER @"Would you like to add this person as?"
// Popup alert to confirm if user wants save the leave when greater than 14 days
#define MSG_LEAVE_LENGTH @"You are trying to enter a leave greater than %i days, tap Yes to confirm?"
// Popup alert to confirm if user wants to remove the leave
#define MSG_LEAVE_DELETE @"Are you sure you want delete the your leave %@?"
// Popup alert to confirm if user have complete the hazard action
#define MSG_HAZARD_ACTION_COMPLETED @"Are you sure you have completed the Hazard action ?"
// Popup alert to confirm if user wants to cancel reporting a hazard
#define MSG_HAZARD_REPORT_CANCEL @"Are you sure you want to cancel the Hazard Report ?"


/////////////// ALERT ////////////////
// Message displaying to user next checkin time
#define MSG_REPORT @"You have checked-in.\n Please report-in by %@"
// Popup alert for local notification to remind user to report in after entity time for report expired
#define MSG_REPORT_ALERT @"Please report-in or your team leader will be notified soon"
// Popup alert for local notification to remind user to report in after entity time for report expired
#define MSG_CHECOUT_AUTOMATIC @"You have been check-out due a system instability, please check-in again"
// Popup alert when user try to create a site when they havent joined any entity
#define MSG_ENTITY_NOT_SELECTED @"To create a site you have to part of an entity first"
// Popup alert when user try to join a entity and the code typed don't match database
#define MSG_ENTITY_NOT_FOUND @"No entity found for that code"
// Popup alert when user try to switch entity when he still checked in to site
#define MSG_CHANGE_ENTITY_ERROR @"You can't switch entity, please check-out first then try again"
// Popup alert when user try to log off when he still checked in to site
#define MSG_LOGOFF_ERROR @"You can't logoff. Please clock-out first then try again"
// Popup alert when user token has changed, this happens when user login from diferent device and the token is updated
// When he is log off if he was previous checked in to a site he will be checkout
// It happens to avoid same person using more then 1 device
#define MSG_LOGOFF_TOKEN_ERROR @"You are been logged out, your token has expired. Please login again"
// Popup alert to confirm the user profile picture were saved successfuly
#define MSG_IMAGE_UPLOAD_SUCCESS @"Profile picture saved"
// Popup alert when error occur trying to upload user profile picture
#define MSG_IMAGE_UPLOAD_FAIL @"Error occur, picture not uploaded"
// Message that will be automaticly composed when user tap to SMS user in danger
#define MSG_SMS_USER_SAFE @"SafetyBeat has advised me you might be in danger. Please report-in if you are safe."
// Popup alert when SMS was send successfuly
#define MSG_SMS_SEND_SUCCESS @"SMS sent successful"
// Popup alert when SMS didn't get sent
#define MSG_SMS_SEND_FAIL @"Error occur, SMS not sent"
// Popup alert when user attempt to go from notification to connection made from diferent entity
#define MSG_NOTIFICATION_DIFERENT_ENTITY @"This notification was sent from a diferent entity. Please switch to entity: %@, in order to resolve it."
// Popup alert when user attempt to create a group message with only 1 user
#define MSG_MESSAGE_GROUP_ONLY_ONE @"Please select more than one user to create a group message"
// Popup alert when user attempt to create a group message without name
#define MSG_MESSAGE_GROUP_NO_NAME @"Please enter the message name"
// Popup alert when user finish download a file
#define MSG_DOCUMENT_OPEN_AFTER_DOWNLOAD @"Download completed, would you like to open the file?"
// Popup alert when document already exist local
#define MSG_DOCUMENT_EXIST @"Document with same name exist local, Would you like to overwrite it?"
// Popup alert when user choose to save file with a new name
#define MSG_DOCUMENT_NEW_NAME @"Please give a new name for the document"
// Popup alert when user try to give an invalid name to the file
#define MSG_DOCUMENT_NEW_NAME_INVALID @"Invalid name, please give a new name for the document"
// Banner will show when user has not performed action in the past 24 hours
#define MSG_MY_TEAM_NO_ACTION @"%@ hasn't perform an action in the last 24 hours"
// Banner will show when user is not found on leader team
#define MSG_MY_TEAM_NO_FOUND @"%@ not found in your %@ team"

// Local Notification when user attempt to report in from notification center been out of geofence
#define TXT_ACTION_NOTE @"Action note, use this to justify a late clock-in or clock-out."

#endif
