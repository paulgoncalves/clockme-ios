//
//  BlinkViewController.h
//  FootyFormPredictions
//
//  Created by Paulo Goncalves on 25/03/2014.
//  Copyright (c) 2014 Anythingnet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKViewController : UIViewController

+(CKViewController *)sharedInstance;
-(void)showClockInView:(UIView *)aView;
-(void)removeClock;

@end
