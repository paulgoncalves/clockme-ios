//
//  SBCoreLocation.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 27/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IngeoSDK/IngeoSDK.h>

@protocol CKCoreLocationDelegate <NSObject>

@optional
-(void)DidFinishGettingUserLocationChange:(BOOL)locationChanged;
-(void)UserLocationIsEnable:(BOOL)locationEnable andAlways:(BOOL)locationAways;
-(void)DidFinishWithError:(NSString *)error;

@end

@interface CKCoreLocation : NSObject <IGLocationManagerDelegate>

@property (nonatomic, retain) id <CKCoreLocationDelegate> delegate;

+(CKCoreLocation *)sharedInstance;
-(void)initShareLocationManager;
-(void)StopShareLocationManager;
-(void)RebuildShareLocationManager;
-(void)GetUserLocationPermission;
+(BOOL)isValidLocation:(CLLocation *)location;

@end
