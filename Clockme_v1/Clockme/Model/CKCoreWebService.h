//
//  SBCore.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 29/01/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/sysctl.h>

/**
 Delegate method for the class
 */
@protocol CKCoreDelegate <NSObject>

@optional
-(void)DidFinishDownloading:(NSDictionary *)dictionary;
-(void)DidFinishDownloadingWithError:(NSString *)error;
-(void)UserTokenExpired;
-(void)DidFinishUploading;
-(void)DidFinishUploadingWithError:(NSString *)error;

@end

@interface CKCoreWebService : NSObject <NSURLSessionDelegate>

@property (nonatomic, retain) id <CKCoreDelegate> delegate;
+(CKCoreWebService *)sharedInstance;

/////////////// CORE SERVICE ///////////////
-(void)CoreWebService:(NSString *)route withRequest:(NSString *)request;
-(void)CoreWebService:(NSString *)route withJsonRequest:(NSString *)myRequestString;
-(void)UploadImage:(UIImage *)tempImg toFolder:(NSString *)route andOption:(NSString *)option;

/////////////// CORE SERVICE BACKGROUND ///////////////
-(void)CoreWebServiceInBackground:(NSString *)route withRequest:(NSString *)myRequestString;
-(void)CoreWebServiceWithoutDelegate:(NSString *)route withRequest:(NSString *)myRequestString;

/////////////// LOG ///////////////
-(void)WriteLog:(NSString *)log;
-(void)WriteActionLog:(NSMutableDictionary *)userAction;

@end
