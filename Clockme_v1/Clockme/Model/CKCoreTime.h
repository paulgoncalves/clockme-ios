//
//  SBCoreTime.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKCoreTime : NSObject

+(CKCoreTime *)sharedInstance;

/////////////// TIME ///////////////
+(BOOL)isStillAvailable:(double)time;
+(BOOL)isTime:(double)time lessThen:(double)days;
+(double)GetCurrentTime;
+(NSDate *)GetDateFromString:(NSString *)date;
+(NSString *)ConvertToLocalTime:(double)time;
+(NSString *)ConvertToLocalTimeDate:(double)time;
+(NSString *)ConvertToLocalTimeWithSeconds:(double)time;
+(NSString *)GetTimeFromDate:(NSString *)stringDate;
+(NSString *)GetHoursByTimeStamp:(double)time;
+(NSString *)GetCountDownTimeByTimeStamp:(double)time;
+(NSString *)GetTimeAgoFromTimeStamp:(double)time;

@end
