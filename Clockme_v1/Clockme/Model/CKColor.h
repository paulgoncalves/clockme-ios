//
//  SBColor.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 6/07/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKColor : UIColor

+(UIColor *)CKDefaultColor;
+(UIColor *)CKLightBlueColor;
+(UIColor *)CKLightGrayColor;
+(UIColor *)CKVeryLightGrayColor;
+(UIColor *)CKOrangeColor;
+(UIColor *)CKGreenColor;


+(UIColor *)CKYellowColor;
+(UIColor *)CKBannerColor;
+(UIColor *)CKLightRedColor;

@end
