//
//  CKActionModel.m
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import "CKActionModel.h"

@implementation CKActionModel

-(id)initObjectWithDictionary:(NSDictionary *)actionInfo{
    
    @try {
        
        self.personId = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"personId"]];
        self.actionId = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"actionId"]];
        self.companyId = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"companyId"]];
        self.placeId = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"placeId"]];
        self.actionType = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"actionType"]];
        
        self.placeName = [CKUtility getStringvalueForKey:[actionInfo valueForKey:@"placeName"]];
        self.timezone = [CKUtility getStringvalueForKey:[actionInfo valueForKey:@"timezone"]];
        
        self.time = [CKUtility getDoubleValueForKey:[actionInfo valueForKey:@"time"]];
        self.location = [[CLLocation alloc] initWithLatitude:[CKUtility getDoubleValueForKey:[actionInfo valueForKey:@"lat"]] longitude: [CKUtility getDoubleValueForKey:[actionInfo valueForKey:@"lng"]]];
        self.onRadius = [CKUtility getBooleanValueForKey:[actionInfo valueForKey:@"onRadius"]];
        
        if ([actionInfo valueForKey:@"actionNoteId"]) {
            self.actionNoteId = [CKUtility getIntValueForKey:[actionInfo valueForKey:@"actionNoteId"]];
            self.note = [CKUtility getStringvalueForKey:[actionInfo valueForKey:@"note"]];
            self.noteTimezone = [CKUtility getStringvalueForKey:[actionInfo valueForKey:@"noteTimezone"]];
            self.noteTime = [CKUtility getDoubleValueForKey:[actionInfo valueForKey:@"noteTime"]];
        }
        
        return self;
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

+(void)scheduleLocalNotificationWithInterval:(NSTimeInterval)interval andAlert:(NSString *)alert withDefaultSound:(BOOL)defaultSound{
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    [localNotif setFireDate:[[NSDate date] dateByAddingTimeInterval:interval]];
    [localNotif setTimeZone:[NSTimeZone defaultTimeZone]];
    [localNotif setAlertBody:alert];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:alert forKey:@"alert"];
    [dic setValue:kDefault_sound forKey:@"sound"];
    [dic setValue:[NSNumber numberWithInt:CKPerson.personId] forKey:@"personId"];
    [localNotif setUserInfo:[NSDictionary dictionaryWithObjectsAndKeys:dic,@"aps",nil]];
    
    [localNotif setAlertAction:@"View Details"];
    [localNotif setApplicationIconBadgeNumber:[[UIApplication sharedApplication]applicationIconBadgeNumber] + 1];
    
    if (!defaultSound) {
        localNotif.soundName = kDefault_sound;
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
}

+(void)UserClockoutSuccess{
    
    //Remove all previous schedule notification
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    CKPlace.actionId = 0;
    CKPlace.name = nil;
    CKPlace.isClockin = FALSE;
    CKPlace.isOffsite = FALSE;
    CKPlace.placeId = 0;
    CKPlace.usedTime = 0;
    CKPlace.radius = 0;
    [CKPlaceModel UpdateSiteInfo];
    
}


@end
