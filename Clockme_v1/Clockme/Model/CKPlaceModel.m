//
//  SBSiteModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 26/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKPlaceModel.h"

@implementation CKPlaceModel

-(id)initObjectWithDictionary:(NSDictionary *)placeInfo{
    
    @try {
        
        self.name = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"placeName"]];
        self.stName = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"stName"]];
        self.stNumber = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"stNumber"]];
        self.suburb = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"suburb"]];
        self.state = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"state"]];
        self.actionId = [CKUtility getIntValueForKey:[placeInfo valueForKey:@"actionId"]];
        self.placeId = [CKUtility getIntValueForKey:[placeInfo valueForKey:@"placeId"]];
        self.radius = [CKUtility getIntValueForKey:[placeInfo valueForKey:@"radius"]];
        self.usedTime = [CKUtility getDoubleValueForKey:[placeInfo valueForKey:@"usedTime"]];
        self.isClockin = [CKUtility getBooleanValueForKey:[placeInfo valueForKey:@"isClockIn"]];
        self.isOffsite = [CKUtility getBooleanValueForKey:[placeInfo valueForKey:@"isOffsite"]];
        self.location = [[CLLocation alloc] initWithLatitude:[CKUtility getDoubleValueForKey:[placeInfo valueForKey:@"lat"]] longitude:[CKUtility getDoubleValueForKey:[placeInfo valueForKey:@"lng"]]];
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:placeInfo];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setValue:dataObj forKey:kCKPlaceModel];
        }else{
            //Clockout user
            [CKActionModel UserClockoutSuccess];
        }
        
        return self;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)initSBSiteFromMemory{
    
    @try {
        
        NSData *archaviedData = [[NSUserDefaults standardUserDefaults] valueForKey:kCKPlaceModel];
        if (archaviedData) {
            NSMutableDictionary *arraySBSite = [NSKeyedUnarchiver unarchiveObjectWithData:archaviedData];
            if ([arraySBSite count] != 0) {
                CKPlace = [self initObjectWithDictionary:arraySBSite];
            }else{
                //Clockout user
                [CKActionModel UserClockoutSuccess];
            }
        }else{
            //Support for users that are migrating from the previous version
            CKPlace = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKPlaceModel]];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        //Support for users that are migrating from the previous version
        CKPlace = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKPlaceModel]];
    }
}

+(void)UpdateSiteInfo{
    
    @try {
        
        NSMutableDictionary *updatePlace = [[NSMutableDictionary alloc] init];
        [updatePlace setValue:CKPlace.name forKey:@"placeName"];
        [updatePlace setValue:CKPlace.stName forKey:@"stName"];
        [updatePlace setValue:CKPlace.stNumber forKey:@"stNumber"];
        [updatePlace setValue:CKPlace.suburb forKey:@"suburb"];
        [updatePlace setValue:CKPlace.state forKey:@"state"];
        [updatePlace setValue:[NSNumber numberWithInt:CKPlace.actionId] forKey:@"actionId"];
        [updatePlace setValue:[NSNumber numberWithInt:CKPlace.placeId] forKey:@"placeId"];
        [updatePlace setValue:[NSNumber numberWithInt:CKPlace.radius] forKey:@"radius"];
        [updatePlace setValue:[NSNumber numberWithDouble:CKPlace.usedTime] forKey:@"usedTime"];
        [updatePlace setValue:[NSNumber numberWithBool:CKPlace.isOffsite] forKey:@"isOffsite"];
        [updatePlace setValue:[NSNumber numberWithBool:CKPlace.isClockin] forKey:@"isClockIn"];
        [updatePlace setValue:[NSNumber numberWithDouble:CKPlace.location.coordinate.latitude] forKey:@"lat"];
        [updatePlace setValue:[NSNumber numberWithDouble:CKPlace.location.coordinate.longitude] forKey:@"lng"];
        NSLog(@"----> Updated site model <----");
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:updatePlace];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setValue:dataObj forKey:kCKPlaceModel];
        }else{
            //Clockout user
            [CKActionModel UserClockoutSuccess];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

-(id)createObjectWithDictionary:(NSDictionary *)placeInfo{
    
    @try {
        
        self.name = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"placeName"]];
        self.stName = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"stName"]];
        self.stNumber = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"stNumber"]];
        self.suburb = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"suburb"]];
        self.state = [CKUtility getStringvalueForKey:[placeInfo valueForKey:@"state"]];
        self.placeId = [CKUtility getIntValueForKey:[placeInfo valueForKey:@"placeId"]];
        self.radius = [CKUtility getIntValueForKey:[placeInfo valueForKey:@"radius"]];
        self.location = [[CLLocation alloc] initWithLatitude:[CKUtility getDoubleValueForKey:[placeInfo valueForKey:@"lat"]] longitude:[CKUtility getDoubleValueForKey:[placeInfo valueForKey:@"lng"]]];
        self.distance = [CKPerson.location distanceFromLocation:self.location];
        
        return self;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

@end
