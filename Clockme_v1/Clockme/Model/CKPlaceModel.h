//
//  SBSiteModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 26/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKPlaceModel : NSObject

@property (nonatomic, copy) NSString *name,*stName,*stNumber,*suburb,*state;
@property double usedTime,distance;
@property (nonatomic, retain) CLLocation *location;
@property int actionId,placeId,radius;
@property BOOL isOffsite,isClockin;

-(id)initObjectWithDictionary:(NSDictionary *)placeInfo;
-(void)initSBSiteFromMemory;
+(void)UpdateSiteInfo;
-(id)createObjectWithDictionary:(NSDictionary *)placeInfo;

@end
