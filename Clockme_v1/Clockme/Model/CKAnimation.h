//
//  SBAnimation.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImage+ImageEffects.h"

@interface CKAnimation : NSObject

//SBAnimation styles
typedef NS_OPTIONS(NSInteger, SBAnimationStyle) {
    SBAnimationStyleLight,
    SBAnimationStyleDark,
    SBAnimationStyleWhiter
};

+(CKAnimation *)sharedInstance;

+(UIImage *)CreateBlurBackground:(UIView *)copyView andEffect:(SBAnimationStyle)style;
+(UIViewController *)showAnimateView:(NSString *)viewName onTopOf:(UIViewController *)view;
+(void)hideAnimateView:(UIViewController *)view;

+(void)FlashView:(UIView *)view;
+(void)RemoveFlashFromView:(UIView *)aview withColor:(UIColor *)aColor;

/////////////// TEXT FORMAT ///////////////
+(CGFloat)TextSize:(NSString *)str withFontSize:(CGFloat)currentSize forLabel:(CGRect)labelRect;

@end
