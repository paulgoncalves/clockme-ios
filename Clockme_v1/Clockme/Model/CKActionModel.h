//
//  CKActionModel.h
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKActionModel : NSObject

@property int personId,actionId,companyId,placeId,actionType;
@property (nonatomic, copy) NSString *placeName,*timezone;
@property (nonatomic, copy) CLLocation *location;
@property double time;
@property BOOL onRadius;

//Action Note
@property int actionNoteId;
@property (nonatomic, copy) NSString *note,*noteTimezone;
@property double noteTime;

-(id)initObjectWithDictionary:(NSDictionary *)actionInfo;
+(void)scheduleLocalNotificationWithInterval:(NSTimeInterval)interval andAlert:(NSString *)alert withDefaultSound:(BOOL)defaultSound;
+(void)UserClockoutSuccess;

typedef enum{
    CKActionTypeClockIn=1,
    CKActionTypeClockOut
}CKActionType;

@end
