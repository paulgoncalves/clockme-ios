//
//  SBEntityModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/02/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKCompanyModel : NSObject

@property int companyId,adminId;
@property double normalStartTime,normalEndTime,afterHourRate;
@property (copy, nonatomic) NSString *companyName;

+(CKCompanyModel *)shareInstance;
-(void)initSBEntityFromMemory;
-(id)initObjectWithDictionary:(NSDictionary *)companyInfo;

+(void)UpdateCompanyInfo;

@end
