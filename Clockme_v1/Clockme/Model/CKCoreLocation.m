//
//  SBCoreLocation.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 27/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKCoreLocation.h"

@implementation CKCoreLocation{
    CLLocationManager *locationManager;
}

static CKCoreLocation *sharedInstance = nil;

+(CKCoreLocation *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

-(void)initShareLocationManager{
    
    NSLog(@"SBCoreLocation ==> getting user location");
    [IGLocationManager initWithDelegate:self secretAPIKey:@"4459e08478970a360c10540c8863ce1d"];
    [IGLocationManager setIngeoBackendSupport:FALSE];
    [IGLocationManager startUpdatingLocation];
    
}

#pragma CORELOCATION

/**
 *  Method will validate if user had giving location persimission to fetch location and call the main class
 */
-(void)GetUserLocationPermission{
    
    //Check if user is running on iOS 8 or above
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            if (status == kCLAuthorizationStatusAuthorizedAlways ) {
                [self.delegate UserLocationIsEnable:TRUE andAlways:TRUE];
            }else{
                [self.delegate UserLocationIsEnable:TRUE andAlways:FALSE];
            }
        }else{
            
            [self initShareLocationManager];
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
                [self.delegate UserLocationIsEnable:FALSE andAlways:FALSE];
            }
        }
    }else{
        
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusAuthorized) {
            [self.delegate UserLocationIsEnable:TRUE andAlways:TRUE];
        }else{
            
            [self initShareLocationManager];
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:kFirstRun]) {
                [self.delegate UserLocationIsEnable:FALSE andAlways:FALSE];
            }
        }
        
    }
    
}

- (void)igLocationManager:(IGLocationManager *)manager didUpdateLocation:(IGLocation *)igLocation{
    
    NSLog(@"SBCoreLocation ==> current location: %@",[IGLocationManager currentLocation]);
    
#if TARGET_IPHONE_SIMULATOR
    CKPerson.location = [[CLLocation alloc] initWithLatitude:fakeLat longitude:fakeLng];
#else
    //Get current user location
    [CKPerson setLocation:[IGLocationManager currentLocation]];
#endif
    
    [self.delegate DidFinishGettingUserLocationChange:TRUE];
    
    if (CKPlace.isClockin) {
        [self CheckUserIsOnSiteGeoFence];
    }else{
        //Update user info after checkin if user left geofence
        [CKPersonModel UpdateUserInfo];
    }

}

-(void)CheckUserIsOnSiteGeoFence{
        
    if ([self CheckUserLeftSiteRadius]) {
        
        [CKActionModel scheduleLocalNotificationWithInterval:0 andAlert:[NSString stringWithFormat:@"%@ It seems that you left the %@, please clock-out",CKPerson.name,CKPlace.name] withDefaultSound:YES];
        CKPerson.leftGeofence = TRUE;
    
    }else{
        CKPerson.leftGeofence = FALSE;
    }
    
    [CKPersonModel UpdateUserInfo];
}

-(BOOL)CheckUserLeftSiteRadius{
    
    //return TRUE;
    
    double distance = [CKPerson.location distanceFromLocation:CKPlace.location];
    if (distance < (CKPlace.radius + ALLOWED_RADIUS)) {
        return FALSE;
    }else{
        return TRUE;
    }
    
}

/**
 *  Remove background track command
 */
-(void)StopShareLocationManager{
    
    [IGLocationManager initWithDelegate:self secretAPIKey:@"b4ef183317dbb4b101ac3c7533066b2c"];
    [IGLocationManager stopUpdatingLocation];
    
}

/** 
 * Method will stop and start the location service again to force retrieve correct location
 **/
-(void)RebuildShareLocationManager{
    [self StopShareLocationManager];
    [self initShareLocationManager];
}

+(BOOL)isValidLocation:(CLLocation *)location{
    if (location.coordinate.latitude == 0 || location.coordinate.longitude == 0) {
        return false;
    }else{
        return true;
    }
}

@end
