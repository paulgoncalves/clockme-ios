//
//  SBCore.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 29/01/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKCoreWebService.h"

@implementation CKCoreWebService {
    UIViewController *mainView;
    UIViewController *heartBeatView;
}


static CKCoreWebService *sharedInstance = nil;

+(CKCoreWebService *)sharedInstance{
    
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

/**
 Method to send POST request
 
 @param route           route to send the request to
 @param myRequestString POST request
 */
-(void)CoreWebService:(NSString *)route withRequest:(NSString *)myRequestString{
    
    NSLog(@" ( WS ) - Request route: %@",route);
    NSLog(@" ( WS ) - Request parameter: %@",myRequestString);
    
    @try {
        //Check if user is connected to the internet
        //if ([self isConnected]) {
            
            NSString *url = [NSString stringWithFormat:@"http://%@/%@",HOST,route];
            
            NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
            [request setHTTPMethod: @"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            [request setHTTPBody:myRequestData];
            NSError *error;
            NSURLResponse *response;
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (error == nil){
                
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
                
                if (dictionary) {
                    
                    NSLog(@" ( WS ) - Answer: %@",[dictionary valueForKey:WebserviceKeyMessage]);
                    
                    if([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginAccessDenied) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kUserTokenExpired];
                        [self.delegate UserTokenExpired];
                    }else{
                        [self.delegate DidFinishDownloading:dictionary];
                    }
                    
                }else{
                    
                    #if TARGET_IPHONE_SIMULATOR
                        NSLog(@"Error: %@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
                        [self.delegate DidFinishDownloadingWithError:MSG_FETCH_ERROR];
                    #else
                        [self WriteLog:[NSString stringWithFormat:@"\n\r UserId: %i \n\r Time: %@ \n\r Error: %@ \n\r -------------------------------------------------------------",CKPerson.personId,[CKCoreTime ConvertToLocalTime:[CKCoreTime GetCurrentTime]],[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]]];
                        [self.delegate DidFinishDownloadingWithError:MSG_FETCH_ERROR];
                    #endif

                }
                
            }else{
                
                [self.delegate DidFinishDownloadingWithError:[error localizedDescription]];
                
            }
            
//        }else{
//            [self.delegate DidFinishDownloadingWithError:MSG_NO_INTERNET];
//        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        [self.delegate DidFinishDownloadingWithError:exception.description];
    }
}

/**
 Method to send data using json encode
 
 @param route           route to send the request to
 @param myRequestString json request
 */
-(void)CoreWebService:(NSString *)route withJsonRequest:(NSString *)myRequestString{

    NSLog(@" ( WS ) - Request route: %@",route);
    NSLog(@" ( WS ) - Request parameter: %@",myRequestString);
    
    @try {
        //check if user is connected to the internet
        //if ([self isConnected]) {
            
            NSString *url = [NSString stringWithFormat:@"http://%@/%@",HOST,route];
            
            NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length] + 10];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
            [request setHTTPMethod: @"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setHTTPBody:myRequestData];
            NSError *error;
            NSURLResponse *response;
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (error == nil){
                
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
                
                if (dictionary) {
                    if([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginAccessDenied) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kUserTokenExpired];
                        [self.delegate UserTokenExpired];
                    }else{
                        [self.delegate DidFinishDownloading:dictionary];
                    }
                    
                }else{
                    
                    #if TARGET_IPHONE_SIMULATOR
                        NSLog(@"Error: %@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
                        [self.delegate DidFinishDownloadingWithError:MSG_FETCH_ERROR];
                    #else
                        [self WriteLog:[NSString stringWithFormat:@"\n\r UserId: %i \n\r Time: %@ \n\r Error: %@ \n\r -------------------------------------------------------------",CKPerson.personId,[CKCoreTime ConvertToLocalTime:[CKCoreTime GetCurrentTime]],[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]]];
                        [self.delegate DidFinishDownloadingWithError:MSG_FETCH_ERROR];
                    #endif
                }
                
            }else{
                
                [[CKViewController sharedInstance] removeClock];
                [self.delegate DidFinishDownloadingWithError:[error localizedDescription]];
                
            }
            
//        }else{
//            [self.delegate DidFinishDownloadingWithError:MSG_NO_INTERNET];
//        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        [self.delegate DidFinishDownloadingWithError:exception.description];
    }
    
}

-(void)CoreWebServiceInBackground:(NSString *)route withRequest:(NSString *)myRequestString{
    
    NSLog(@" ( WS background ) - Request route: %@",route);
    NSLog(@" ( WS ) - Request parameter: %@",myRequestString);
    
    @try {
        //Check if user is connected to the internet
        //if ([self isConnected]) {
            
            NSString *url = [NSString stringWithFormat:@"http://%@/%@",HOST,route];
            
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[myRequestString dataUsingEncoding:NSUTF8StringEncoding]];
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
                
                if (error == nil){
                    
                    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
                    
                    if (dictionary) {
                        [self.delegate DidFinishDownloading:dictionary];
                        NSLog(@"Finished background task: %@",[dictionary valueForKey:WebserviceKeyMessage]);
                    }else{
                        //NSLog(@"Error on background task: %@",dictionary);
                        NSLog(@"Error: %@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
                    }
                    
                }else{

                    NSLog(@"Error: %@",error);
                    
                }

            }] resume];
            
//        }else{
//            NSLog(@"Error: %@",MSG_NO_INTERNET);
//        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

/**
 Method to send POST request
 
 @param route           route to send the request to
 @param myRequestString POST request
 */
-(void)CoreWebServiceWithoutDelegate:(NSString *)route withRequest:(NSString *)myRequestString{
    
    @try {
        //Check if user is connected to the internet
        //if ([self isConnected]) {
            
            NSString *url = [NSString stringWithFormat:@"http://%@/%@",HOST,route];
            
            NSData *myRequestData = [NSData dataWithBytes:[myRequestString UTF8String] length:[myRequestString length]];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
            [request setHTTPMethod: @"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            [request setHTTPBody:myRequestData];
            NSError *error;
            NSURLResponse *response;
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            if (error == nil){
                
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
                
                if (dictionary) {
                    NSLog(@"Result: %@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
                }
                
            }else{
                NSLog(@"Error: %@",[error localizedDescription]);
            }
            
//        }else{
//            NSLog(@"Error: %@",MSG_NO_INTERNET);
//        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}


/**
 Method to upload image to server
 */
-(void)UploadImage:(UIImage *)tempImg toFolder:(NSString *)route andOption:(NSString *)option{
    
    NSLog(@" ( WS ) - Request image upload to: %@",route);
    
    NSData *imageData;
    NSString * filenames;
    
    @try {
        
        //Define the folder route to save the image
        if ([route isEqualToString:@"userImage"]) {
            imageData = UIImageJPEGRepresentation(tempImg,0.2);
            filenames = [NSString stringWithFormat:@"%i&%@&%@",CKPerson.personId,CKPerson.token,route];
        }else if ([route isEqualToString:@"support"]){
            imageData = UIImageJPEGRepresentation(tempImg,0.1);
            filenames = [NSString stringWithFormat:@"%i&%@&%@",CKPerson.personId,CKPerson.token,route];
        }else if ([route isEqualToString:@"hazard"]){
            //The hazardId used as file name is sent with the route detail
            imageData = UIImageJPEGRepresentation(tempImg,0.2);
            filenames = [NSString stringWithFormat:@"%i&%@&%@&%@",CKPerson.personId,CKPerson.token,route,option];
        }else if ([route isEqualToString:@"hazardAction"]){
            //The hazardActionId used as file name is sent with the route detail
            imageData = UIImageJPEGRepresentation(tempImg,0.2);
            filenames = [NSString stringWithFormat:@"%i&%@&%@&%@",CKPerson.personId,CKPerson.token,route,option];
        }
        
        if (imageData != nil){
            //NSLog(@"%@", filenames);
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/imgUpload",HOST]]];
            
            [request setHTTPMethod:@"POST"];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"filenames\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[filenames dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Disposition: form-data; name=\"userfile\"; filename=\".jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *returnData, NSError *connectionError) {
                
                if (connectionError){
                    NSLog(@"Image upload error: %@", connectionError);
                    [self.delegate DidFinishUploadingWithError:MSG_IMAGE_UPLOAD_FAIL];
                }else{
                    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:nil];
                    
                    if ([dictionary count] != 0) {
                        if([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LoginAccessDenied) {
                            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kUserTokenExpired];
                            [self.delegate UserTokenExpired];
                        }else{
                            NSLog(@"Success: %@",dictionary);
                            [self.delegate DidFinishUploading];
                        }
                        
                    }else{
                        NSLog(@"Server response:%@",response);
                        NSLog(@"Server response:%@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
                        [self.delegate DidFinishUploadingWithError:MSG_IMAGE_UPLOAD_FAIL];
                    }
                }
            }];
            
        }else{
            NSLog(@"No image sent to upload");
            [self.delegate DidFinishUploadingWithError:MSG_IMAGE_UPLOAD_FAIL];
        }
    }
    @catch (NSException *exception) {
        [self.delegate DidFinishUploadingWithError:MSG_IMAGE_UPLOAD_FAIL];
        NSLog(@"Error: %@",exception);
    }
    
    [[CKViewController sharedInstance] removeClock];
    
}

/**
 Method to write a string into file log.txt in root api directory
 This method will call the corewebservice without a callback
 @param log string with log
 */
-(void)WriteLog:(NSString *)log{
    NSLog(@"Write to log.txt: %@",log);
    NSString *myRequestString = [NSString stringWithFormat:@"message=%@",log];
    [self CoreWebServiceWithoutDelegate:kRouteLog withRequest:myRequestString];
}

/**
 Method to write to a actionLog table with the user action made via app
 
 @param log array with action detail
 */
-(void)WriteActionLog:(NSMutableDictionary *)userAction{
    
    NSString *myRequestString = @"";
    NSArray *keys = [userAction allKeys];
    
    for (NSString *key in keys) {
        myRequestString = [NSString stringWithFormat:@"%@%@=%@&",myRequestString,key,[userAction valueForKey:key]];
    }
    
    [self CoreWebService:kRouteLogAction withRequest:myRequestString];
}

@end
