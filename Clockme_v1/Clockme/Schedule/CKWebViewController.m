//
//  SBWebViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/06/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKWebViewController.h"

@interface CKWebViewController ()

@end

@implementation CKWebViewController

@synthesize webView,url;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSURL *urlPerson = [NSURL URLWithString:[NSString stringWithFormat:@"http://clockme.co/admin/fullcalendar/demos/simple.html"]];
    //NSURL *urlPerson = [NSURL URLWithString:[NSString stringWithFormat:@"http://35dec7d5.ngrok.io/planner"]];
    [webView loadRequest:[[NSURLRequest alloc] initWithURL:urlPerson]];
    [webView setScalesPageToFit:TRUE];
    
}

-(IBAction)ShowMenu:(id)sender{
    
    CKMenuViewController *CKMenu = (CKMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [[CKViewController sharedInstance] showClockInView:self.view];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[CKViewController sharedInstance] removeClock];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
