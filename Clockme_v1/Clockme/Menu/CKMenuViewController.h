//
//  CKMenuViewController.h
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKMenuViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *imgBg;
@property (nonatomic, strong) UIImage *imgReceive;
@property (nonatomic, weak) IBOutlet UIButton *btnClockIn;
@property (nonatomic, weak) IBOutlet UIButton *btnSchedule;
@property (nonatomic, weak) IBOutlet UIButton *btnProfile;
@property (nonatomic, weak) IBOutlet UIButton *btnMyTeam;
@property (nonatomic, weak) IBOutlet UIButton *btnLogOff;
@property (nonatomic, weak) IBOutlet UIButton *btnCancel;
@property (nonatomic, weak) IBOutlet UILabel *lblCompanyName;

-(IBAction)ClockIn:(id)sender;
-(IBAction)Schedule:(id)sender;
-(IBAction)Profile:(id)sender;
-(IBAction)MyTeam:(id)sender;

//Bottom Menu
-(IBAction)Cancel:(id)sender;
-(IBAction)LogOff:(id)sender;


@end
