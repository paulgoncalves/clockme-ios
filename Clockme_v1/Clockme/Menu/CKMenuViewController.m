//
//  CKMenuViewController.m
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import "CKMenuViewController.h"

@interface CKMenuViewController ()

@end

@implementation CKMenuViewController

@synthesize imgBg,imgReceive;
@synthesize btnClockIn,btnSchedule,btnProfile,btnMyTeam,btnLogOff;
@synthesize lblCompanyName;

- (void)viewDidLoad {
    [super viewDidLoad];
    [imgBg setImage:imgReceive];
}

-(void)viewWillAppear:(BOOL)animated{
    [lblCompanyName setText:CKCompany.companyName];
    
    if (CKPerson.accessLevel == CKPersonManager) {
        [btnMyTeam setUserInteractionEnabled:TRUE];
        [btnMyTeam setAlpha:1];
    }else{
        [btnMyTeam setUserInteractionEnabled:FALSE];
        [btnMyTeam setAlpha:0.5];
    }
}

-(IBAction)ClockIn:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"HomeView"]];
    //[self dismissView];
}

-(IBAction)Schedule:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"WebView"]];
    //[self dismissView];
}

-(IBAction)Profile:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ProfileView"]];
    //[self dismissView];
}

-(IBAction)MyTeam:(id)sender{
    [self presentView:[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"TeamStatusView"]];
    //[self dismissView];
}

-(IBAction)Cancel:(id)sender{
    [self dismissView];
}

-(IBAction)LogOff:(id)sender{
    if (!CKPlace.isClockin) {
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    }else{
        [[[UIAlertView alloc] initWithTitle:nil message:MSG_LOGOFF_ERROR delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
    
}

-(void)presentView:(UIViewController *)view{
    [view setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
