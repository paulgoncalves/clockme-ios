#import "AnnotationView.h"

@implementation AnnotationView

@synthesize title,subtitle,coordinate;


- (id)initWithLocation: (CLLocationCoordinate2D) coord {
    coordinate=coord;
    return self;
}

-(CLLocationCoordinate2D)coord
{
    return coordinate;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    coordinate = newCoordinate;
    [self.delegate DidGotNewLocation:newCoordinate];
}

@end
