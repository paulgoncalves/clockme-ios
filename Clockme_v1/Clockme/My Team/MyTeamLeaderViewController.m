//
//  ViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "MyTeamLeaderViewController.h"

@interface MyTeamLeaderViewController (){
    CKCoreWebService *CK;
    NSMutableArray *array,*arrayMyTeam,*arrayMyTeamAnnotation,*arraySearch;
    BOOL isSearching;
}

@end

@implementation MyTeamLeaderViewController

@synthesize mapView,btnRefreshLocation,actInd;
@synthesize btnBackground,searchBar;

- (void)viewDidLoad{
    [super viewDidLoad];

    //Set title for view
    [self setTitle:@"Team status"];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    
    [mapView setDelegate:self];
    [searchBar setDelegate:self];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [self CleanMap];
    
    arrayMyTeam = [[NSMutableArray alloc] init];
    
    //Notification to refresh user location after user return from background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(start) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(IBAction)ShowMenu:(id)sender{
    
    CKMenuViewController *CKMenu = (CKMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

/**
 Method to start getting user location before fetching the user team
 
 @param animated default
 */
-(void)viewDidAppear:(BOOL)animated{
    
    //Only load again if not team is found
    if ([arrayMyTeam count] == 0) {
        [self start];
    }else{
        [self CleanMap];
        [self LoadMap];
    }
    
}

/**
 Method to fetch all the sites available for entity, after retrieving it sort for sites arround person
 */
-(void)FetchData{
    
    array = [[NSMutableArray alloc] init];
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&companyId=%i&accessLevel=%i",CKPerson.personId,CKPerson.token,CKCompany.companyId,CKPerson.accessLevel];
    [CK CoreWebService:kRouteTeamMyTeam withRequest:myRequestString];
    
}

/**
 Method to refresh the view and fetch new data
 
 @param sender default
 */
-(IBAction)start{
    
    //Disable button
    [btnRefreshLocation setEnabled:FALSE];
    [btnRefreshLocation setImage:[UIImage imageNamed:@"location_pressed.png"] forState:UIControlStateNormal];
    [actInd startAnimating];
    
    [[CKViewController sharedInstance] showClockInView:self.view];
    
    [self CleanMap];
    [self performSelectorInBackground:@selector(FetchData) withObject:nil];
}


#pragma -mark SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_MyTeamSuccess) {
            
            for (NSDictionary *dic in [dictionary valueForKey:@"team"]) {
                [array addObject:[[CKCompanyPersonModel alloc] initObjectWithDictionary:dic]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self LoadMap];
            });
            
        }
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self CleanMap];
            [self StopAnimation];
            
            [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        });
        
        
    }
}

/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self StopAnimation];
        [self RefreshMap];
        [[CKViewController sharedInstance] removeClock];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
    
}

-(void)StopAnimation{
    
    @try {
        [actInd stopAnimating];
        [btnRefreshLocation setEnabled:TRUE];
        [btnRefreshLocation setImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
        [[CKViewController sharedInstance] removeClock];
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

#pragma SORT LOCATION BY DISTANCE
-(void)LoadMap{
    
    NSArray *arrayLoad;
    
    if (isSearching) {
        arrayLoad = arraySearch;
        isSearching = FALSE;
    }else{
        arrayLoad = array;
    }
    
    @try {
        
        for (CKCompanyPersonModel *myTeamModel in arrayLoad) {
            
            //If user location exist
            if (myTeamModel.location) {
                //Verify if got user latitude before loading him to map
                if (myTeamModel.location.coordinate.latitude != 0) {
                    
                    REVClusterPin *note = [[REVClusterPin alloc] init];
                    note.title = myTeamModel.name;
                    note.subtitle = [NSString stringWithFormat:@"%@ at %@",myTeamModel.placeName,[CKCoreTime ConvertToLocalTime:myTeamModel.time]];
                    note.coordinate = myTeamModel.location.coordinate;
                    [arrayMyTeamAnnotation addObject:note];
                    
                    //Save the locations into array
                    [arrayMyTeam addObject:myTeamModel];
                    
                }
                
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self RefreshMap];
    });
}

/**
 Method to get called in main thread to refresh map with pins
 */
-(void)RefreshMap{
    
    //Enable button
    [self StopAnimation];
    
    if ([arrayMyTeam count] != 0) {
        [mapView showAnnotations:[arrayMyTeamAnnotation copy] animated:YES];
    }else{
        [self CenterMap];
    }
    
}

-(void)CenterMap{
    //Set my location to center the map
    MKCoordinateRegion local = MKCoordinateRegionMakeWithDistance(CKPerson.location.coordinate, 800, 800);
    local.center.latitude = CKPerson.location.coordinate.latitude - 0.009;
    local.center.longitude = CKPerson.location.coordinate.longitude;
    local.span.latitudeDelta = 0.04f;
    local.span.longitudeDelta = 0.04f;
    [mapView setRegion:[mapView regionThatFits:local] animated:YES];
}

-(void)CleanMap{
    
    arraySearch = [[NSMutableArray alloc] init];
    arrayMyTeam = [[NSMutableArray alloc] init];
    arrayMyTeamAnnotation = [[NSMutableArray alloc] init];
    [mapView removeAnnotations:[[mapView annotations] copy]];
    
}

#pragma -mark MapViewDelegate
/**
 Method to handle the pin property. This method assing a custom pin to site and user.
 
 @param mV         default
 @param annotation default
 
 @return default
 */
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    
    @try {
        
        REVClusterPin *pin = (REVClusterPin *)annotation;
        
        MKAnnotationView *annView;
        
        if([annotation class] == MKUserLocation.class) {
            return annView;
        }
        
        if( [pin nodeCount] > 0 ){
            pin.title = @"___";
            annView = (REVClusterAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
            
            if( !annView )
                annView = (REVClusterAnnotationView*)
                [[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
            
            annView.image = [UIImage imageNamed:@"cluster.png"];
            
            [(REVClusterAnnotationView*)annView setClusterText:
             [NSString stringWithFormat:@"%i",(int)[pin nodeCount]]];
            
            annView.canShowCallout = NO;
            
        } else {
            
            annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"currentloc"];
            
            if( !annView )
                annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
            
            UIButton *btnFriendInfo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            
            @try {
                
                [btnFriendInfo setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
                annView.rightCalloutAccessoryView = btnFriendInfo;
                annView.canShowCallout = YES;
                
                int x = 0;
                
                for (MKPointAnnotation *point in [arrayMyTeamAnnotation copy]) {
                    
                    if (point == annotation) {
                        
                        CKCompanyPersonModel *myTeamModel = [arrayMyTeam objectAtIndex:x];
                        //Friend pin
                        btnFriendInfo.tag = x;
                        
                        if (myTeamModel.actionType == CKActionTypeClockIn) {
                            annView.image = [UIImage imageNamed:[NSString stringWithFormat:@"pin_person_green.png"]];
                        }else{
                            annView.image = [UIImage imageNamed:[NSString stringWithFormat:@"pin_person_grey.png"]];
                        }
                        
                        break;
                        
                    }else{
                        x++;
                    }
                }
                
            }
            @catch (NSException *exception) {
                NSLog(@"Error: %@",exception);
            }
        }
        
        return annView;
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
}

- (void)mapView:(MKMapView *)mapVieww didSelectAnnotationView:(MKAnnotationView *)view{
    
    if (![view isKindOfClass:[REVClusterAnnotationView class]])
        return;
    
    CLLocationCoordinate2D centerCoordinate = [(REVClusterPin *)view.annotation coordinate];
    MKCoordinateSpan newSpan = MKCoordinateSpanMake(mapView.region.span.latitudeDelta/2.0, mapView.region.span.longitudeDelta/2.0);
    [mapView setRegion:MKCoordinateRegionMake(centerCoordinate, newSpan) animated:YES];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [mapView removeAnnotations:mapView.annotations];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}


#pragma -mark SEARCHBAR DELEGATE
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [btnBackground setHidden:FALSE];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        [arrayMyTeam removeAllObjects];
        [arrayMyTeamAnnotation removeAllObjects];
        [mapView removeAnnotations:[mapView annotations]];
        isSearching = false;
        [self LoadMap];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBarr{
    
    [arraySearch removeAllObjects];
    [btnBackground setHidden:TRUE];
    
    NSString *stringText = searchBarr.text;
    
    @try {
        
        if (stringText.length > 0) {
            
            for(CKCompanyPersonModel *myTeamModel in arrayMyTeam) {
                NSRange substringRange = [myTeamModel.name rangeOfString:stringText options:NSCaseInsensitiveSearch];
                if (substringRange.location != NSNotFound) {
                    [arraySearch addObject:myTeamModel];
                }
            }
        }else{
            [arraySearch removeAllObjects];
            isSearching = false;
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    
    if ([arraySearch count] != 0) {
        
        isSearching = true;
        [arrayMyTeam removeAllObjects];
        [arrayMyTeamAnnotation removeAllObjects];
        [mapView removeAnnotations:[mapView annotations]];
        [self LoadMap];
        
    }else{
        
        if ([self UserExists:stringText]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:MSG_MY_TEAM_NO_ACTION delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }else{
            [[[UIAlertView alloc] initWithTitle:@"" message:MSG_MY_TEAM_NO_FOUND delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
    
    [searchBarr resignFirstResponder];
}

-(BOOL)UserExists:(NSString *)userName{
    
    @try {
        
        for(NSArray *arr in arrayMyTeam) {
            NSString *str = [NSString stringWithFormat:@"%@",[arr valueForKey:@"name"]];
            NSRange substringRange = [str rangeOfString:userName options:NSCaseInsensitiveSearch];
            if (substringRange.location != NSNotFound) {
                return TRUE;
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return FALSE;
    
}

-(IBAction)Background:(id)sender{
    [searchBar resignFirstResponder];
    [btnBackground setHidden:TRUE];
}

/**
 Remove notification when view is dissapeared
 
 @param animated default
 */
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[CKViewController sharedInstance] removeClock];
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
