//
//  ViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AnnotationView.h"
#import "REVClusterMapView.h"
#import "REVClusterMap.h"
#import "REVClusterAnnotationView.h"
#import "CKCompanyPersonModel.h"

@interface MyTeamLeaderViewController : UIViewController <MKMapViewDelegate,UISearchBarDelegate,CKCoreDelegate,CKCoreLocationDelegate>

@property (nonatomic) IBOutlet REVClusterMapView *mapView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (weak,nonatomic) IBOutlet UIActivityIndicatorView *actInd;

@property (nonatomic, weak) IBOutlet UIButton *btnBackground;
@property (nonatomic) IBOutlet UIButton *btnRefreshLocation;

-(IBAction)Background:(id)sender;
-(IBAction)ShowMenu:(id)sender;

@end
