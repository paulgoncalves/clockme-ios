//
//  MyTeamModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 3/12/2015.
//  Copyright © 2015 com.alerton.com. All rights reserved.
//

#import "CKCompanyPersonModel.h"

@implementation CKCompanyPersonModel

static CKCompanyPersonModel *shareInstance;

+(CKCompanyPersonModel *)shareInstance{
    if (shareInstance == NULL) {
        shareInstance = [[CKCompanyPersonModel alloc] init];
    }
    
    return shareInstance;
}

-(id)initObjectWithDictionary:(NSDictionary *)myTeam{
    
    self.personId = [CKUtility getIntValueForKey:[myTeam valueForKey:@"personId"]];
    self.email = [CKUtility getStringvalueForKey:[myTeam valueForKey:@"email"]];
    self.name = [CKUtility getStringvalueForKey:[myTeam valueForKey:@"name"]];
    self.mobile = [CKUtility getStringvalueForKey:[myTeam valueForKey:@"mobile"]];
    self.companyId = [CKUtility getIntValueForKey:[myTeam valueForKey:@"companyId"]];
    self.accessLevel = [CKUtility getIntValueForKey:[myTeam valueForKey:@"accessLevel"]];
    
    //Location
    NSDictionary *dicLocaton = [[myTeam valueForKey:@"personLocation"] objectAtIndex:0];
    
    if (dicLocaton) {
        self.placeId = [CKUtility getIntValueForKey:[dicLocaton valueForKey:@"placeId"]];
        self.actionId = [CKUtility getIntValueForKey:[dicLocaton valueForKey:@"actionId"]];
        self.actionType = [CKUtility getIntValueForKey:[dicLocaton valueForKey:@"actionType"]];
        self.placeName = [CKUtility getStringvalueForKey:[dicLocaton valueForKey:@"placeName"]];
        self.time = [CKUtility getDoubleValueForKey:[dicLocaton valueForKey:@"time"]];
        self.timezone = [CKUtility getStringvalueForKey:[dicLocaton valueForKey:@"timezone"]];
        self.location = [[CLLocation alloc] initWithLatitude:[CKUtility getDoubleValueForKey:[dicLocaton valueForKey:@"lat"]] longitude:[CKUtility getDoubleValueForKey:[dicLocaton valueForKey:@"lng"]]];
    }
    
    return self;
}

@end
