#import <Foundation/Foundation.h>
#import "MapKit/MKAnnotation.h"

/**
 Delegate method for the class
 */
@protocol AnnotationViewDelegate <NSObject>

@optional
-(void)DidGotNewLocation:(CLLocationCoordinate2D)newLocation;
@end

@interface AnnotationView : NSObject <MKAnnotation>{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, retain) id <AnnotationViewDelegate> delegate;
@property (nonatomic, assign) CLLocationCoordinate2D businessCoordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, assign) NSInteger tag;

- (id)initWithLocation:(CLLocationCoordinate2D)coord;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
