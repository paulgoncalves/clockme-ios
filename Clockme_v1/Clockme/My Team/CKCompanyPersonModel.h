//
//  MyTeamModel.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 3/12/2015.
//  Copyright © 2015 com.alerton.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CKCompanyPersonModel : NSObject

@property int accessLevel,personId,companyId;
@property (nonatomic, strong) NSString *email,*name,*mobile;

//Location
@property int placeId,actionId,actionType;
@property double time;
@property (nonatomic, strong) NSString *placeName,*timezone;
@property (nonatomic, strong) CLLocation *location;

+(CKCompanyPersonModel *)shareInstance;
-(id)initObjectWithDictionary:(NSDictionary *)myTeam;

@end
