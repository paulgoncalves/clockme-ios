//
//  CKActionNoteViewController.h
//  Clockme
//
//  Created by Paulo Goncalves on 23/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CKActionNoteDelegate <NSObject>

@optional
-(void)DidShowKeyboard;
-(void)DidHideKeyboard;
-(void)DidFinishWithActionNote;
@end

@interface CKActionNoteViewController : UIViewController <UITextViewDelegate,CKCoreDelegate>

@property (nonatomic, retain) id <CKActionNoteDelegate> delegate;
@property (nonatomic,weak) IBOutlet UITextView *txtActionNote;

@property (nonatomic,weak) IBOutlet UILabel *lblAction;
@property (nonatomic,weak) IBOutlet UILabel *lblPlace;
@property (nonatomic,weak) IBOutlet UILabel *lblTime;

@property (nonatomic, retain) CKActionModel *actionModel;

-(IBAction)Save:(id)sender;
-(IBAction)Cancel:(id)sender;
-(IBAction)Background:(id)sender;
-(void)ShowActionNote;
-(void)HideActionNote;


@end