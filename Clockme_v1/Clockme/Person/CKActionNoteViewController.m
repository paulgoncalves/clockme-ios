//
//  CKActionNoteViewController.m
//  Clockme
//
//  Created by Paulo Goncalves on 23/03/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKActionNoteViewController.h"

@interface CKActionNoteViewController (){
    CKCoreWebService *CK;
}

@end

@implementation CKActionNoteViewController

@synthesize txtActionNote;
@synthesize lblAction,lblPlace,lblTime;
@synthesize actionModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    [txtActionNote setDelegate:self];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (actionModel.actionType == CKActionTypeClockIn) {
        [lblAction setText:@"Clock-in"];
    }else{
        [lblAction setText:@"Clock-out"];
    }
    
    [lblPlace setText:actionModel.placeName];
    [lblTime setText:[CKCoreTime ConvertToLocalTime:actionModel.time]];
    
    if (actionModel.actionNoteId) {
        [txtActionNote setText:actionModel.note];
    }else{
        [txtActionNote setText:TXT_ACTION_NOTE];
    }

}

/**
 Method to request save leave
 */
-(void)RequestSaveNote{
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&note=%@&actionId=%i&ts=%f&tz=%@",CKPerson.personId,CKPerson.token,txtActionNote.text,CKPlace.actionId,[CKCoreTime GetCurrentTime],CKPerson.timezone];
    [CK CoreWebService:kRouteActionNote withRequest:myRequestString];
}

#pragma mark - SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_ActionNoteSaveSuccess) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                [self.delegate DidFinishWithActionNote];
            });
            
        }
        
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        });
        
    }
    
    [[CKViewController sharedInstance] removeClock];
}


/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKViewController sharedInstance] removeClock];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
    
}
-(IBAction)Save:(id)sender{
    
    if (txtActionNote.text.length != 0) {
        
        [[CKViewController sharedInstance] showClockInView:self.view];
        [self performSelectorInBackground:@selector(RequestSaveNote) withObject:nil];
        
    }else{
        
        [[[UIAlertView alloc] initWithTitle:@"" message:MSG_REGISTER_ALL_FIELDS delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    }
}

-(IBAction)Background:(id)sender{
    [txtActionNote resignFirstResponder];
}

-(void)Cancel:(id)sender{
    [self.delegate DidFinishWithActionNote];
}

#pragma -mark TextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:TXT_ACTION_NOTE]) {
        [txtActionNote setText:@""];
    }else{
        if (textView.text.length == 0) {
            [txtActionNote setText:TXT_ACTION_NOTE];
        }
    }
}

#pragma -mark PickerViewDelegate

-(void)ShowActionNote{
    [UIView animateWithDuration:0.2 animations:^() {
        [self.view setAlpha:1];
    }];
}

-(void)HideActionNote{
    [UIView animateWithDuration:0.2 animations:^() {
        [self.view setAlpha:0];
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[CKViewController sharedInstance] removeClock];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
