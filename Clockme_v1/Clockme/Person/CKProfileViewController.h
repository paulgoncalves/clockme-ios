//
//  ViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "CKTableViewCell.h"
#import "CKActionNoteViewController.h"

@interface CKProfileViewController : UIViewController <UIAlertViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CKCoreDelegate,CKActionNoteDelegate>

@property (strong, nonatomic) NSArray *arrayProfile;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITableView *tblContent;

-(IBAction)Background:(id)sender;
-(IBAction)ShowMenu:(id)sender;

@end
