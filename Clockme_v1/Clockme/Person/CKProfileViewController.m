//
//  ViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 18/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKProfileViewController.h"

@interface CKProfileViewController (){
    CKCoreWebService *CK;
    NSMutableArray *arrayAction;
    UIImageView *imgBlurBg;
    CKActionNoteViewController *actionNoteView;
    
}

@end

@implementation CKProfileViewController

@synthesize txtEmail,txtName,txtMobile;
@synthesize arrayProfile,tblContent;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    //Set title for view
    [self setTitle:@"My Profile"];
    
    //[btnSave setHidden:TRUE];
    [txtEmail setDelegate:self];
    [txtName setDelegate:self];
    [txtMobile setDelegate:self];
    
    [self DisableTextField];
    
    arrayAction = [[NSMutableArray alloc] init];
}

/**
 Method to fetch user info, if a profileId is assign it will look upon the user detail, otherwise it will load the app user profile.
 
 TODO: if user is admin it will allow him to click to view user realated files, this to be implemented on next version
 
 @param animated default
 */
-(void)viewDidAppear:(BOOL)animated{
    
    [CK setDelegate:self];
    txtEmail.text = CKPerson.email;
    txtName.text = CKPerson.name;
    txtMobile.text = CKPerson.mobile;
    txtEmail.enabled = false;
    
    [self performSelectorInBackground:@selector(RequestPersonActions) withObject:nil];
    
}

-(IBAction)ShowMenu:(id)sender{
    
    CKMenuViewController *CKMenu = (CKMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

/**
 Method to Fetch data with user log
 */
-(void)RequestPersonActions{

    NSString *myRequestString;
    int limit = 20;
    
    if ([arrayAction count] == 0) {
        myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&limit=%i",CKPerson.personId,CKPerson.token,limit];
    }else{
        //Get last actionId
        CKActionModel *actionModel = [arrayAction objectAtIndex:(int)[arrayAction count]-1];
        myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&&limit=%i&actionId=%i",CKPerson.personId,CKPerson.token,limit,actionModel.actionId];
    }
    
    [CK CoreWebService:kRoutePersonActions withRequest:myRequestString];
}

#pragma mark - SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_LogUserSuccess) {
            
            //Sort array by user status
            NSSortDescriptor *sortAction = [[NSSortDescriptor alloc] initWithKey:@"actionId" ascending:YES comparator:^(id first, id second) {
                return [first compare:second options:NSNumericSearch];
            }];

            NSArray *tempArray = [[dictionary valueForKey:@"actions"]  sortedArrayUsingDescriptors:@[sortAction]];
            
            for (NSDictionary *dic in tempArray) {
                [arrayAction addObject:[[CKActionModel alloc] initObjectWithDictionary:dic]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [tblContent reloadData];
            });
            
        }
        
    }else{
     
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] != SBWebServiceResponseCode_LogNoLog) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"" message:[dictionary valueForKey:WebserviceKeyMessage] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }

    }
    
    [[CKViewController sharedInstance] removeClock];
}


/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKViewController sharedInstance] removeClock];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayAction count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *myIdentifier = @"cell";
    
    CKTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[CKTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    //Validate if cell has been proper initialized before continue
    if (!cell) {
        return nil;
    }
    
    @try {
        
        CKActionModel *actionModel = [arrayAction objectAtIndex:indexPath.row];
        
        [cell.lblHeaderTitle setText:[NSString stringWithFormat:@"Place: %@",actionModel.placeName]];
        [cell.lblHeaderSubtitle setText:[NSString stringWithFormat:@"Time: %@",[CKCoreTime ConvertToLocalTime:actionModel.time]]];
        
        if (actionModel.actionType == CKActionTypeClockIn) {
            [cell.imgAction setImage:[UIImage imageNamed:@"in.png"]];
        }else{
            [cell.imgAction setImage:[UIImage imageNamed:@"out.png"]];
        }
        
        [cell.txtActionNote setText:actionModel.note];
        
        //Manage cell elements
        [cell.lblFooterSubtitle setHidden:TRUE];
        [cell.lblFooterTitle setHidden:TRUE];
        [cell.txtActionNote setHidden:FALSE];
        
        [cell.btnAction setTag:indexPath.row];
        [cell.btnAction addTarget:self action:@selector(AddActionNote:) forControlEvents:UIControlEventTouchUpInside];
     
        //Load more data after reaching the bottom
        if ([arrayAction count] - 1 == indexPath.row) {
            [self RequestPersonActions];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return cell;
}


-(void)AddActionNote:(UIButton *)sender{
    
    //Show action note
    actionNoteView = (CKActionNoteViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"ActionNoteView"];
    [actionNoteView setDelegate:self];
    [actionNoteView.view setCenter:CGPointMake(self.view.frame.size.width / 2, 300)];

    [actionNoteView setActionModel:[arrayAction objectAtIndex:sender.tag]];
    [self ShowAddActionNoteView];
    
}

#pragma -mark ActionNoteDelegate
-(void)DidFinishWithActionNote{
    [self HideActionNoteView];
}


#pragma -mark ShowPassword change
-(void)ShowAddActionNoteView{
    [self AddBlurBg];
    [actionNoteView ShowActionNote];
    [self.view addSubview:actionNoteView.view];
}

-(void)HideActionNoteView{
    [imgBlurBg setHidden:TRUE];
    imgBlurBg = nil;
    [self Background:self];
    
    [actionNoteView HideActionNote];
}

-(void)AddBlurBg{
    if (imgBlurBg == nil) {
        
        @try {
            imgBlurBg = [[UIImageView alloc] initWithImage:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleWhiter]];
            if (imgBlurBg) {                
                [UIView animateWithDuration:0.2 animations:^() {
                    [imgBlurBg setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    [imgBlurBg setAlpha:0];
                    [self.view insertSubview:imgBlurBg belowSubview:actionNoteView.view];
                    [imgBlurBg setAlpha:1];
                }];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Error: %@",exception);
        }
    }
}

/**
 Method delegate for UIAlertView
 
 @param alertView   alert
 @param buttonIndex button pressed
 */
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}

/**
 Methdo to enable all editable textField
 */
-(void)EnableTextField{
    [txtName setEnabled:TRUE];
    [txtMobile setEnabled:TRUE];
    [txtEmail setEnabled:TRUE];
    
    //Set white color background
    [txtName setBackgroundColor:[UIColor whiteColor]];
    [txtMobile setBackgroundColor:[UIColor whiteColor]];
    [txtEmail setBackgroundColor:[UIColor whiteColor]];
}

/**
 Methdo to disable all editable textField
 */
-(void)DisableTextField{
    [txtName setEnabled:FALSE];
    [txtMobile setEnabled:FALSE];
    [txtEmail setEnabled:FALSE];
    
    //Set white color background
    [txtName setBackgroundColor:[CKColor lightGrayColor]];
    [txtMobile setBackgroundColor:[CKColor lightGrayColor]];
    [txtEmail setBackgroundColor:[CKColor lightGrayColor]];}

/**
 Method when user tap on background
 
 @param sender default
 */
-(IBAction)Background:(id)sender{
    [txtEmail resignFirstResponder];
    [txtName resignFirstResponder];
    [txtMobile resignFirstResponder];
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[CKViewController sharedInstance] removeClock];
}
@end
