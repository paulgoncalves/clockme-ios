//
//  SBUserModel.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 23/02/2015.
//  Copyright (c) 2015 com.alerton.com. All rights reserved.
//

#import "CKPersonModel.h"

@implementation CKPersonModel

// Import method to initialize the session
-(void)initSBSession{
    self.session = TRUE;
    self.timezone = [[NSTimeZone localTimeZone] name];
    
    //Init once only the location service
    [[CKCoreLocation sharedInstance] initShareLocationManager];
}

-(id)initObjectWithDictionary:(NSDictionary *)personInfo{
    
    @try {
        self.personId = [CKUtility getIntValueForKey:[personInfo valueForKey:@"personId"]];
        self.name = [CKUtility getStringvalueForKey:[personInfo valueForKey:@"name"]];
        self.token = [CKUtility getStringvalueForKey:[personInfo valueForKey:@"token"]];
        self.email = [CKUtility getStringvalueForKey:[personInfo valueForKey:@"email"]];
        self.mobile = [CKUtility getStringvalueForKey:[personInfo valueForKey:@"mobile"]];
        self.accessLevel = [CKUtility getIntValueForKey:[personInfo valueForKey:@"accessLevel"]];
        self.leftGeofence = [CKUtility getBooleanValueForKey:[personInfo valueForKey:@"leftGeofence"]];
        self.session = [CKUtility getBooleanValueForKey:[personInfo valueForKey:@"session"]];
        
        if (self.session) {
            //Initiate SB Session
            [self initSBSession];

            //Update database if user timezone changed
            if (![[personInfo valueForKey:@"timezone"] isEqualToString:self.timezone]) {
                //Request server timezone update in background
                //Reuse update device method
                [self RequestUpdateUserTimezone:self];
            }
        }
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:personInfo];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setObject:dataObj forKey:kCKPersonModel];
        }else{
            //Clockout user
            [CKActionModel UserClockoutSuccess];
        }
     
        return self;
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)initCKPersonFromMemory{

    @try {
        
        NSData *archaviedData = [[NSUserDefaults standardUserDefaults] valueForKey:kCKPersonModel];
        if (archaviedData) {
            NSMutableDictionary *arraySBUser = [NSKeyedUnarchiver unarchiveObjectWithData:archaviedData];
            if ([arraySBUser count] != 0) {
                CKPerson = [self initObjectWithDictionary:arraySBUser];
            }else{
                //Clockout user
                [CKActionModel UserClockoutSuccess];
            }
        }else{
            //Support for users that are migrating from the previous version
            CKPerson = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKPersonModel]];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);

        //Support for users that are migrating from the previous version
        CKPerson = [self initObjectWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:kCKPersonModel]];
    }
    
}

+(void)UpdateUserInfo{
 
    @try {
        NSMutableDictionary *updateUser = [[NSMutableDictionary alloc] init];
        [updateUser setValue:[NSNumber numberWithInt:CKPerson.personId] forKey:@"personId"];
        [updateUser setValue:CKPerson.name forKey:@"name"];
        [updateUser setValue:CKPerson.token forKey:@"token"];
        [updateUser setValue:CKPerson.email forKey:@"email"];
        [updateUser setValue:CKPerson.mobile forKey:@"mobile"];
        [updateUser setValue:[NSNumber numberWithInt:CKPerson.accessLevel] forKey:@"accessLevel"];
        [updateUser setValue:[NSNumber numberWithBool:CKPerson.session] forKey:@"session"];
        [updateUser setValue:[NSNumber numberWithBool:CKPerson.leftGeofence] forKey:@"leftGeofence"];
        [updateUser setValue:CKPerson.timezone forKey:@"timezone"];

        NSLog(@"----> Updated user model <----");
        
        //Save info on phone memory
        NSData *dataObj = [NSKeyedArchiver archivedDataWithRootObject:updateUser];
        if (dataObj) {
            [[NSUserDefaults standardUserDefaults] setObject:dataObj forKey:kCKPersonModel];
        }else{
            //Clockout user
            [CKActionModel UserClockoutSuccess];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
}

-(void)RequestUpdateUserTimezone:(CKPersonModel *)SBUserModel{
    
    NSString *myRequestString = [NSString stringWithFormat:@"userId=%i&token=%@&timezone=%@",SBUserModel.personId,SBUserModel.token,SBUserModel.timezone];
    [[CKCoreWebService sharedInstance] CoreWebServiceInBackground:kRouteUserUpdateTimezone withRequest:myRequestString];
    
}

-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_UserTokenUpdateSuccess) {
            NSLog(@" :::::::: Updated timezone on cloud :::::::::");
        }
    }
}

@end
