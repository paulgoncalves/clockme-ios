//
//  UserConnectionViewController.m
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import "CKHomeViewController.h"

@interface CKHomeViewController (){
    CKCoreWebService *CK;
    CKCoreLocation *CKLocation;
    NSMutableArray *array;
    NSTimer *refreshRow;
    NSIndexPath *updateRow;
}

@end

@implementation CKHomeViewController

@synthesize tblContent,imgNoExist;

- (void)viewDidLoad{
    [super viewDidLoad];
    CK = [[CKCoreWebService alloc] init];
    [CK setDelegate:self];
    
    if (CKPerson.session) {
        CKLocation = [[CKCoreLocation alloc] init];
        [CKLocation setDelegate:self];
        [CKLocation GetUserLocationPermission];
    }else{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    }
}

-(IBAction)ShowMenu:(id)sender{

    CKMenuViewController *CKMenu = (CKMenuViewController *)[[CKMain getStoryBoard] instantiateViewControllerWithIdentifier:@"CKMenu"];
    [CKMenu setImgReceive:[CKAnimation CreateBlurBackground:self.view andEffect:SBAnimationStyleLight]];
    [CKMenu setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:CKMenu animated:YES completion:nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
#if TARGET_IPHONE_SIMULATOR
    //Set fake location
    [CKPerson setLocation:[[CLLocation alloc] initWithLatitude:fakeLat longitude:fakeLng]];
    [self FetchData];
#else
    //Get current user location
    [CKPerson setLocation:[IGLocationManager currentLocation]];
    [self FetchData];
#endif
    
}

-(void)DidFinishGettingUserLocationChange:(BOOL)locationChanged{
    [self FetchData];
}

-(void)UserLocationIsEnable:(BOOL)locationEnable andAlways:(BOOL)locationAways{

    if (locationEnable) {
        [CKLocation initShareLocationManager];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"" message:MSG_LOCATION_DISABLE delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
}

-(void)DidFinishWithError:(NSString *)error{
    [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

-(void)Refresh:(id)sender{
    [self FetchData];
}

/**
 Method to fecth data
 */
-(void)FetchData{
    
    [[CKViewController sharedInstance] showClockInView:self.view];
    [self performSelectorInBackground:@selector(RequestPlaces) withObject:nil];
    
}

/**
 *  Method that will fetch all connections for the user
 */
-(void)RequestPlaces{
    
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&companyId=%i&lat=%f&lng=%f",CKPerson.personId,CKPerson.token,CKCompany.companyId,CKPerson.location.coordinate.latitude,CKPerson.location.coordinate.longitude];
    [CK CoreWebService:kRoutePlaceCompany withRequest:myRequestString];
}

/**
 Method to clock in
 */
-(void)RequestClockin:(UIButton *)sender{

    CKPlace = [array objectAtIndex:sender.tag];
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&placeId=%i&companyId=%i&lat=%f&lng=%f&ts=%f&timezone=%@",CKPerson.personId,CKPerson.token,CKPlace.placeId,CKCompany.companyId,CKPerson.location.coordinate.latitude,CKPerson.location.coordinate.longitude,[CKCoreTime GetCurrentTime],CKPerson.timezone];
    [CK CoreWebService:kRouteActionClockin withRequest:myRequestString];
}

/**
 Method to clock in
 */
-(void)RequestClockout:(UIButton *)sender{

    CKPlace = [array objectAtIndex:sender.tag];
    NSString *myRequestString = [NSString stringWithFormat:@"personId=%i&token=%@&placeId=%i&companyId=%i&lat=%f&lng=%f&ts=%f&timezone=%@",CKPerson.personId,CKPerson.token,CKPlace.placeId,CKCompany.companyId,CKPerson.location.coordinate.latitude,CKPerson.location.coordinate.longitude,[CKCoreTime GetCurrentTime],CKPerson.timezone];
    [CK CoreWebService:kRouteActionClockout withRequest:myRequestString];
}

#pragma -mark SBCoreDelegate
/**
 Method delegate to be called after data been retrieve
 
 @param dictionary fetched data
 */
-(void)DidFinishDownloading:(NSDictionary *)dictionary{
    
    if ([[dictionary valueForKey:WebserviceKeyStatus] intValue] == SBWebServiceResponseSuccess) {
        
        if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_PlaceFetchSuccess) {
            
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            
            for (NSMutableDictionary *dic in [dictionary valueForKey:@"places"]) {                
                [tempArray addObject:[[CKPlaceModel alloc] createObjectWithDictionary:dic]];
            }
            
            array = [[self SortPlaceByDistance:tempArray] copy];
            
        }else if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_ActionClockinSuccess) {
            
            CKPlace.actionId = [[dictionary valueForKey:@"actionId"] intValue];
            CKPlace.usedTime = [CKCoreTime GetCurrentTime];
            CKPlace.isClockin = TRUE;
            
            [CKPlaceModel UpdateSiteInfo];
            
        }else if ([[dictionary valueForKey:WebserviceKeyCode] intValue] == SBWebServiceResponseCode_ActionClockoutSuccess) {
            
            CKPlace.isClockin = FALSE;
            
            //Invalidate update row timer
            [refreshRow invalidate];
            refreshRow = nil;
            
            [CKPlaceModel UpdateSiteInfo];
            
        }
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [tblContent reloadData];
        });
        
    }else{
        
        
    }
    
    [[CKViewController sharedInstance] removeClock];
    
}

-(NSArray *)SortPlaceByDistance:(NSArray *)toSort{
    
    @try {
        
        NSSortDescriptor *sortDistance = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES comparator:^NSComparisonResult(id obj1, id obj2){
            CKPlaceModel *CKPlace1 = (CKPlaceModel *)obj1;
            CKPlaceModel *CKPlace2 = (CKPlaceModel *)obj2;
            
            return [[NSNumber numberWithDouble:CKPlace1.distance] compare:[NSNumber numberWithDouble:CKPlace2.distance]];
            
        }];
        
        return [toSort sortedArrayUsingDescriptors:@[sortDistance]];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
        return nil;
    }
}


/**
 Method delegate called if retrieving data failed
 
 @param error message of error
 */
-(void)DidFinishDownloadingWithError:(NSString *)error{
    
    [[CKViewController sharedInstance] removeClock];
    dispatch_async(dispatch_get_main_queue(), ^{
        [tblContent reloadData];
        [[[UIAlertView alloc] initWithTitle:@"" message:error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

#pragma -mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *myIdentifier = @"cell";
    
    CKActionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIdentifier];
    if (cell == nil) {
        cell = [[CKActionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIdentifier];
    }
    
    //Validate if cell has been proper initialized before continue
    if (!cell) {
        return nil;
    }
    
    @try {
        
        CKPlaceModel *placeModel = [array objectAtIndex:indexPath.row];

        [cell.lblPlaceName setText:[NSString stringWithFormat:@"Place: %@",placeModel.name]];
        [cell.lblAcceptedRadius setText:[NSString stringWithFormat:@"Accept distance: %d",placeModel.radius]];
        
        [cell.lblDistance setText:[NSString stringWithFormat:@"Distance: %.2f meters",placeModel.distance]];
        
        if (CKPlace.isClockin && CKPlace.placeId == placeModel.placeId) {
            
            [cell.btnAction setHidden:FALSE];
            [cell.lblAcceptedRadius setHidden:TRUE];
            
            //Change button to Clockout
            [cell.btnAction setBackgroundColor:[CKColor CKOrangeColor]];
            [cell.btnAction setTitle:@"Clock-out" forState:UIControlStateNormal];
            [cell.btnAction setTag:indexPath.row];
            [cell.btnAction addTarget:self action:@selector(RequestClockout:) forControlEvents:UIControlEventTouchUpInside];
            
            //Add count from used time
            [cell.lblDistance setText:[NSString stringWithFormat:@"Duration: %@",[CKCoreTime GetTimeAgoFromTimeStamp:CKPlace.usedTime]]];
            
            if (!refreshRow) {
                updateRow = indexPath;
                refreshRow = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateTableRowAtIndex) userInfo:nil repeats:YES];
            }
            
        }else{
            
            [cell.lblAcceptedRadius setHidden:FALSE];
            
            if (placeModel.distance < placeModel.radius) {
                [cell.btnAction setHidden:FALSE];
                [cell.btnAction setTag:indexPath.row];
                [cell.btnAction setBackgroundColor:[CKColor CKGreenColor]];
                [cell.btnAction setTitle:@"Clock-in" forState:UIControlStateNormal];
                [cell.btnAction addTarget:self action:@selector(RequestClockin:) forControlEvents:UIControlEventTouchUpInside];
            }else{
                [cell.btnAction setHidden:TRUE];
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Error: %@",exception);
    }
    
    return cell;
    
}

-(void)updateTableRowAtIndex{
    [tblContent reloadRowsAtIndexPaths:[NSArray arrayWithObjects:updateRow,nil] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"DetailUserSegue"]) {
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}
/**
 Method to return to Team mapview
 */
-(void)MapView{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)DismissView{
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  Method to call when user token has expired
 */
-(void)UserTokenExpired{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[CKLoginModel sharedInstance] LogOffUserWithAccessDenied:self];
    });
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[CKViewController sharedInstance] removeClock];
}

@end
