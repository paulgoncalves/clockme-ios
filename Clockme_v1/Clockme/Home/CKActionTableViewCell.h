//
//  CKActionTableViewCell.h
//  Clockme
//
//  Created by iMac on 31/12/2015.
//  Copyright © 2015 Braziosdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKActionTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblPlaceName;
@property (nonatomic, weak) IBOutlet UILabel *lblAcceptedRadius;
@property (nonatomic, weak) IBOutlet UILabel *lblDistance;

@property (nonatomic, weak) IBOutlet UIButton *btnAction;

@end
