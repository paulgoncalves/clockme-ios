//
//  UserConnectionViewController.h
//  SafetyBeat
//
//  Created by Paulo Goncalves on 25/08/2014.
//  Copyright (c) 2014 com.alerton.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKActionTableViewCell.h"

@interface CKHomeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,CKCoreDelegate,CKCoreLocationDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoExist;

-(IBAction)ShowMenu:(id)sender;
-(IBAction)Refresh:(id)sender;

@end
